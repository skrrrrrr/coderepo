let apis = require('./routes/apis.js');

let chai = require('chai');
let chaiHttp = require('chai-http');
let server = require('./server.js');
let should = chai.should();

var express = require('express');
const router = express.Router();
var session = require('express-session');


const ObjectID=require('mongodb').ObjectID;
const MongoClient = require('mongodb').MongoClient;
const url = "mongodb+srv://kyle1234:kyle12345@final-year-project01-islcf.mongodb.net/finalgroupproject?retryWrites=true";

var req = require('supertest');
var expect = require('chai').expect;
chai.use(chaiHttp);


const userCredentials = [{

  username: 'Dawn',
  password: 'password1'}
  ,
  {username: 'No',
    password: 'p'}
]
//now let's login the user before we run any tests
var authenticatedUser = req.agent(server);
before(function(done){
  authenticatedUser
    .post('/api/login')
    .send(userCredentials)
    .end(function(err, response){
      expect(response.statusCode).to.equal(200);
      expect('Location','localhost:4200/admin-home/');
      done();
    });
});
describe('Login test', function(done){
  //addresses 1st bullet point: if the user is logged in we should get a 200 status code
    it('should return 200 response if the user is logged in', function()
    {
      authenticatedUser.post('localhost:4200/admin-home')
      .expect(200, done);
    });
    it('should return a 302 response and redirect to /login', function()
    {
      req(server).post('localhost:4200/admin-home')
      .expect('Location','localhost:4600/api/login')
      .expect(302, done);
    });

  });
  describe("/Worker details", () => {
    describe("GET /", () => {
        // Test to get all workers
        it("should get all workers records", (done) => {
             req(server)
                 .get('/api/users')
                 .end((err, res) => {
                     res.should.have.status(200);
                     res.should.be.a('object');
                     //res.should.have(result);
                     done();
                  });
         });
        // Test to get single user record
        /*it("should get a single worker record", (done) => {
             const id = 1;
             req(server)
                 .get(`/api/userDetail`, '${id}')
                 .end((err, res) => {
                     res.should.have.status(200);
                     res.should.be.a('object');
                     done();
                  });
         });*/

        // Test to Not single user record
        /*it("should NOT get a single user record", (done) => {
             const id = -50000000;
             req(server)
                 .get(`/api/userDetail`, '${id}')
                 .end((err, res) => {
                   res.should.have.status(404);
                     expect(null);
                     done();
                  });
         });*/

    });
});
    describe("/unit details", () => {
      describe("GET /", () => {
          // Test to get all workers
          it("should get all units", (done) => {
               req(server)
                   .get('/api/units')
                   .end((err, res) => {
                       res.should.have.status(200);
                       //res.should.be.a('object');
                       //res.should.have(result);
                       done();
                    });
           });

           it("should get a single unit record", (done) => {
                const id = "5c69ce7b98ce4412aca929b9";
                req(server)
                    .get(`/api/unit/`, '5c69ce7b98ce4412aca929b9')
                    .end((err, res) => {
                        //res.should.have.status(200);
                        res.should.be.a('object');
                        done();
                     });
            });
            it("should NOT get a single unit record", (done) => {
                 const id = -50000000;
                 req(server)
                     .get(`/api/unit/`, '${id}')
                     .end((err, res) => {
                       res.should.have.status(404);
                         expect(null);
                         done();
                      });
             });
});
});
describe("/ticket details", () => {
  describe("GET /", () => {
            it("should get all tickets", (done) => {
                 req(server)
                     .get('/api/issues')
                     .end((err, res) => {
                         //res.should.have.status(200);
                         res.should.be.a('object');
                         //res.should.have(result);
                         done();
                      });
             });

             it("should get a single ticket", (done) => {
                  //const id = "5cc8abbf593ad45dbcb72105";
                  req(server)
                      .get(`/api/issue`, '5cc8abbf593ad45dbcb72105')
                      .end((err, res) => {
                          //res.should.have.status(200);
                          res.should.be.a('object');
                          done();
                       });
              });
              it("should NOT get a single ticket", (done) => {
                   const id = -50000000;
                   req(server)
                       .get(`/api/issue`, '${id}')
                       .end((err, res) => {
                        res.should.have.status(404);
                           expect(null);
                           done();
                        });
               });
});
});
describe("/landlord details", () => {
  describe("GET /", () => {
              it("should get all landlords", (done) => {
                   req(server)
                       .get('/api/landlords')
                       .end((err, res) => {
                           res.should.have.status(200);
                           res.should.be.a('object');
                           //res.should.have(result);
                           done();
                        });
               });

               it("should get a single landlord record", (done) => {
                    //const id = "5c6ad0744dc4974bf49860ab";
                    req(server)
                        .get('/api/landlord', '5c6ad0744dc4974bf49860ab')
                        .end((err, res) => {
                            //res.should.have.status(200);
                            res.should.be.a('object');
                            done();
                         });
                });

                it("should NOT get a single landlord record", (done) => {
                     const id = -50000000;
                     req(server)
                         .get('/api/landlord', '${id}')
                         .end((err, res) => {
                          res.should.have.status(404);
                             expect(null);
                             done();
                          });
                 });
});
});
//console.log(req.app);

//this test says: make a POST to the /login route with the email: blah , password: blah
//after the POST has completed, make sure the status code is 200
//also make sure that the user has been directed to the /home pages
//Make sure that incorrect details pass the test as they are expected to not log in and therefor
