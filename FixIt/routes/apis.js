var express = require('express');
const util = require('util');
const router = express.Router();
var session = require('express-session');
var nodemailer = require('nodemailer');
const ObjectID = require('mongodb').ObjectID;
const MongoClient = require('mongodb').MongoClient;
const url = "mongodb+srv://kyle1234:kyle12345@final-year-project01-islcf.mongodb.net/finalgroupproject?retryWrites=true";
const from_address = 'fitix.skrrrrrrr@gmail.com';
const from_address_pass = "Iam,123.";
const new_landlord_email_template = 'Hi %s,\n\nThank you for choosing us, you can now log into our system using the following credentials:\n\nUsername: %s\nPassword: %s\n\nRegaeds,\n\nFixIt';
const new_ticket_email_template = 'Hi %s,\n\nWe are reaching out to you regarding a service request raised on your registered property %s, the ticket details are as follow：\nSubject: %s\nCategory: %s\nDescription: %s\n\nPlease do not heasitate to contact us, if you need any help.\n\nRegards,\nFixIt';
const new_ticket_assigned_email_template = 'Hi %s,\n\nA new ticket has been assigned to you. Please check ASAP.\n\nThanks,\nRegards.';
const ticket_status_update_subject_template = 'Status Change Regarding Your Ticket: %s'
const ticket_status_update_email_template = 'Hi %s,\n\nWe are reaching out to you regarding the update of your Ticket: %s , the change is as follow：\n\t%s\n\nPlease do not heasitate to contact us, if you need any help.\n\nRegards,\nFixIt';
var password_generator = require('generate-password');

//var multer = require('multer');
//const storage = multer.diskStorage({
  //destination: function (req, file, cb) {
    //cb(null, '../../../image/')
  //},
  //filename: function (req, file, cb) {
    //cb(null, file.originalname)
  //}
//})
//const upload = multer({
  //storage: storage,
  //limit: {
    //fileSize: 50 * 1024 * 1024
 // }
//})

var url2 = require('url');

function validateEmail(email) {
  var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(String(email).toLowerCase());
}

function validatePhone(phone) {
  var re = /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im;
  return re.test(phone);
}

function validateFax(fax) {
  var re = new RegExp("^\\+[0-9]{1,3}-[0-9]{3}-[0-9]{7}$");
  return re.test(fax);
}

//login
router.post('/login', function (req, res) {
  var type = req.body.type;
  var username = req.body.email;
  var password = req.body.password;
  req.session.username = username;
  if (username == null || password.length <= 5 || !validateEmail(username)) {
    res.json({ OK: 0, message: "Password or/and email address are incorrect." });
  }
  else {
    MongoClient.connect(url, (err, client) => {
      if (err) throw err;
      var query = { $and: [{ "login_name": username, "password": password }] };
      var db = client.db('finalgroupproject');
      console.log(parseInt(req.body.type));

      if (username == "advancedAdmin@gmail.com" && password == "FixIt2019") {
        db.collection("advanced").find(query).toArray(function (err, result) {
          var msg = { success: 0, message: null };
          if (result.length == 0) {
            msg.message = "Password or/and email address are incorrect.";
            res.send(msg);
          } else {
            //req.session.username = username;
            //req.session.userId = result[0]._id;
            msg.id = result._id;
            msg.success = 1;
            res.send(msg);
          }
        });
        console.log("SUPER");
      }

      else if (type == 1) {
        db.collection("office").find(query).toArray(function (err, result) {
          var msg = { success: 0, message: null };
          if (result.length == 0) {
            msg.message = "Password or/and email address are incorrect.";
            res.send(msg);
          } else {
            //req.session.username = username;
            //req.session.userId = result[0]._id;
            msg.id = result[0]._id;
            msg.success = 1;
            msg.type = 1;
            res.send(msg);
          }
        });
      }

      else if (type == 2) {
        query = { $and: [{ "landlord_email": username, "password": password }] };
        db.collection("landlord").find(query).toArray(function (err, result) {
          var msg = { success: 0, message: null };
          if (result.length == 0) {
            msg.message = "Password or/and email address are incorrect.";
            res.send(msg);
          } else {
            //req.session.username = username;
            //req.session.userId = result[0]._id;
            msg.id = result[0]._id;
            msg.success = 1;
            msg.type = 2;
            console.log(result);

            res.send(msg);
          }
        });
      }
      else if (type == 3) {
        db.collection("tenant").find(query).toArray(function (err, result) {
          var msg = { success: 0, message: null };
          if (result.length == 0) {
            msg.message = "Password or/and email address are incorrect.";
            res.send(msg);
          } else {
            //req.session.username = username;
            //req.session.userId = result[0]._id;
            msg.id = result[0]._id;
            msg.success = 1;
            msg.type = 3;
            res.send(msg);
          }
        });
      }
      else if (type == 4) {
        query = { $and: [{ "contractor_email": username, "password": password }] };

        db.collection("contractor").find(query).toArray(function (err, result) {
          var msg = { success: 0, message: null };
          if (result.length == 0) {
            msg.message = "Password or/and email address are incorrect.";
            res.send(msg);
          } else {
            //req.session.username = username;
            //req.session.userId = result[0]._id;
            msg.id = result[0]._id;
            msg.success = 1;
            msg.type = 4;
            res.send(msg);
          }
        });
      }
      client.close();

    });
  }
});


// GETS ALL WORKER
router.get('/users', function (req, res, next) {
  MongoClient.connect(url, (err, client) => {
    if (err) throw err;
    var db = client.db('finalgroupproject');
    db.collection("office").find().toArray(function (err, result) {
      var msg = { message: "" };

      if (result.value === null) {
        msg['message'] = "No office worker found.";
        res.send(msg);
      } else {
        res.send(result);
      }
    });
    client.close();
  });
});


// GETS SINGLE WORKER USERS
router.get('/userDetail', function (req, res, next) {
  MongoClient.connect(url, (err, client) => {
    if (err) throw err;
    var query = { "_id": (userDetails._id) };
    var db = client.db('finalgroupproject');
    db.collection("office").find(query).toArray(function (err, result) {
      var msg = { message: "" };

      if (result.value === null) {
        msg['message'] = "No user found";
        res.send(msg);
      } else {
        res.send(JSON.stringify(result));
      }
    });
    client.close();
  });
});

router.post('/addUser', function (req, res, next) {
  // gets form information
  MongoClient.connect(url, (err, client) => {
    if (err) throw err;
    var query = {
      "login_name": req.body.username,
      "password": req.body.password,
      "type": req.body.type,
      "phone": req.body.phone,
      "email": req.body.email,
      "fax": req.body.fax,
      "unevailable_period": req.body.unevailable_period
    };

    if (!validateEmail(req.body.username) || !validateEmail(req.body.email) || req.body.password.length <= 5 || !validatePhone(req.body.phone) || !validateFax(req.body.fax)) {
      // console.log("we in validate phone");
      res.json({ OK: 0, message: "Please check the information provided" });
    }
    else {
      var db = client.db('finalgroupproject');
      db.collection("office").insert(query, function (err, result) {
        var msg = { message: null };

        if (result.value === null) {
          msg = { message: "User details not added" };
        }
        else {
          res.send(result);
        };
      });
    }
  });
});

// REMOVE A Users BY name
router.post('/deleteUser', function (req, res, next) {

  MongoClient.connect(url, (err, client) => {
    if (err) throw err;
    var username = req.body.uname2;
    var query = { "login_name": username };
    var db = client.db('finalgroupproject');
    db.collection("office").findOneAndDelete(query, function (err, result) {
      var msg = { message: "" };

      if (result.value === null) {
        msg['message'] = "No user found";
        res.send(msg);
      } else {
        msg = { message: "Successful deletion" };
        res.send(msg);
      }
    });
    client.close();
  });
});

// Update A Users BY Name
router.post('/updateUser', function (req, res, next) {
  MongoClient.connect(url, (err, client) => {
    if (err) throw err;
    var db = client.db('finalgroupproject');
    var newValues = req.body.newUname;

    var query = { "login_name": req.body.login_name };
    db.collection("office").findOneAndUpdate(query, { $set: { "login_name": req.body.login_name, "password": req.body.password, "type": req.body.type, "phone": req.body.phone, "email": req.body.email, "fax": req.body.fax, "unavailable_period": req.body.unavailable_period } }, function (err, result) {
      var msg = { message: "" };

      if (result.value === null) {
        msg['message'] = "No user found";
        res.send(msg);
      } else {
        msg = { message: "Successful update" };
        res.send(msg);
      }
      if (err) throw err;

    });
    client.close();
  });
});


router.post('/addLandlord', function (req, res, next) {
  // gets form information
  MongoClient.connect(url, (err, client) => {
    if (err) throw err;
    console.log(req.body);

    // if (!validateEmail(req.body.email) || !validatePhone(req.body.landlordPhoneMobile) || !validatePhone(req.body.phoneBank) || !validateEmail(req.body.emailBank)) {
    //   res.json({ OK: 0, message: "Please check the information provided" });
    // }

    // else {
      var password = password_generator.generate({
        length: 6,
        numbers: true
      });
      var query = {
        user_type: 2,
        "landlord_name": req.body.landlordName,
        "landlord_phone_work": req.body.landlordPhoneWork,
        "landlord_phone_home": req.body.landlordPhoneHome,
        "landlord_phone_mobile": req.body.landlordPhoneMobile,
        "landlord_email": req.body.email,
        "landlord_address": req.body.landlordAddress,
        "password": password,
        "bank_name": req.body.bankName,
        "bank_address": req.body.bankAddress,
        "bank_BIC": req.body.bic,
        "bank_IBAN": req.body.iban,
        "bank_phone": req.body.phoneBank,
        "bank_email": req.body.emailBank,
        "bank_fax": req.body.fax
      };
      console.log(query);
      var db = client.db('finalgroupproject');
      db.collection("landlord").insert(query, function (err, result) {
        var msg = { message: null };

        if (result.value === null) {
          msg = { message: "Landlord details not added" };
        } else {
          var content = util.format(new_landlord_email_template, req.body.landlordName, req.body.email, password);
          var emailObject = {
            to_address: req.body.email,
            subject: "Welcome From FixIt",
            content
          };

          send_email(emailObject, function(error, info){
            if (error) throw error;
            console.log(info);
            res.send({result,msg});
          })
        };
      });
    // }
  });
});

router.post('/addBankDetails', function (req, res, next) {
  // gets form information
  MongoClient.connect(url, (err, client) => {
    if (err) throw err;
    var query = {
      "bank_name": req.body.bankName,
      "bank_address": req.body.bankAddress,
      "bank_BIC": req.body.bic,
      "bank_IBAN": req.body.iban,
      "bank_phone": req.body.phoneBank,
      "bank_email": req.body.emailBank,
      "bank_fax": req.body.fax,
      "contact_id": null
    };

    if (!validateEmail(req.body.emailBank) || !validatePhone(req.body.fax)) {
      res.json({ OK: 0, message: "Please check the information provided" });
    }

    else {
      var db = client.db('finalgroupproject');
      db.collection("bank").insert(query, function (err, result) {
        var msg = { message: null };

        if (result.value === null) {
          msg = { message: "Bank details not added" };
        } else {
          res.send(result);
        };
      });
    }
  });
});


//GET All UNITS
router.get('/units', function (req, res, next) {
  //use our db collection called users_db
  MongoClient.connect(url, (err, client) => {
    if (err) throw err;
    var db = client.db('finalgroupproject');
    db.collection("unit").find().sort(getNatrulSortOrder('unit', 1)).toArray(function (err, result) {
      if (err) throw err;
      res.send(result);
    });
    client.close();
  });
});

//CREATE UNIT
router.post('/addUnit', function (req, res, next) {
  MongoClient.connect(url, (err, client) => {
    if (err) throw err;
    var ref = new DbRef("landlord", new ObjectID(req.body.landlord_id));
    var query = {
      "unit_name": req.body.unitName,
      "unit_address1": req.body.address1,
      "unit_address2": req.body.address2,
      "unit_address3": req.body.address3,
      "unit_type": req.body.unitType,
      "unit_carspaces": req.body.carSpaces,
      "unit_leasedspaces": req.body.unit_leasedspaces,
      "financial_year_start": req.body.financialYearStart,
      "financial_year_end": req.body.financialYearEnd,
      "vat_registration_no": req.body.vatNo,
      "email_address": req.body.email,
      "web_address": req.body.webAddress,
      "financial_management": req.body.financialManagement,
      "direct_debit_details": req.body.direct_debit_details,
      "contractor_payment_details": req.body.contractor_payment_details,
      "landlord_id": ref
    };

    // if (!validateEmail(req.body.email)) {
    //   res.json({ OK: 0, message: "Please check the information provided" });
    // }

    // else {
      var db = client.db('finalgroupproject');
      db.collection("unit").insert(query, function (err, result) {
        var msg = { message: null };

        if (result.value === null) {
          msg = { message: "Unit details not added" };
        } else {
          res.send(result);
        };
      });
    // }
  });
});
//GET UNIT BY name  5c5442f05d7ceb0488087ddc
router.post('/unit/', function (req, res, next) {
  MongoClient.connect(url, (err, client) => {
    if (err) throw err;
    var query = { "_id": new ObjectID(req.body.id) };
    var db = client.db('finalgroupproject');
    db.collection("unit").findOne(query, function (err, result) {
      var msg = { message: "" };
      if (result.value === null) {
        msg['message'] = "No unit found";
        res.send(msg);
      } else {

        res.send(JSON.stringify(result));
      }
    });
    client.close();
  });
});

// REMOVE A UNIT BY ID
router.post('/properties/remove/', function (req, res, next) {

  MongoClient.connect(url, (err, client) => {
    if (err) throw err;
    var db = client.db('finalgroupproject');
    var query = { "_id": new ObjectID(req.body.id) };
    db.collection("unit").findOneAndDelete(query, function (err, result) {
      var msg = { message: "" };

      if (result.value === null) {
        msg['message'] = "No property found.";
      } else {

        msg['message'] = "Property removed.";
      }
      res.send(msg);
    });
    client.close();
  });
});

// GET ALL RAISED TICKETS
router.post('/issues', function (req, res, next) {

  MongoClient.connect(url, (err, client) => {
    if (err) throw err;
    var db = client.db('finalgroupproject');
    var sort = getNatrulSortOrder('ticket', 1);
    console.log(typeof (req.body.sortOption));

    if (req.body.sortOption != null) {
      switch (parseInt(req.body.sortOption)) {
        case 1:
          sort = { priority: 1 };
          break;

        case 2:
          sort = { priority: -1 };
          break;

        case 4:
          sort = getNatrulSortOrder("ticket", -1);
          break;

        case 5:
          sort = { time_created: 1 };
          break;

        case 6:
          sort = { time_created: -1 };
          break;

        default:
          break;
      }
    }
    console.log(sort);

    db.collection("ticket").find().sort(sort).toArray(function (err, result) {
      if (err) throw err;
      res.send(result);
    });
    client.close();
  });
});

// UPDATE A PROPERTY
router.post('/properties/update/', function (req, res, next) {

  MongoClient.connect(url, (err, client) => {
    if (err) throw err;
    var db = client.db('finalgroupproject');
    var query = { "_id": new ObjectID(req.body.id) };
    var newValues = {
      "unit_name": req.body.propertyObject.unitName,
      "unit_address1": req.body.propertyObject.address1,
      "unit_address2": req.body.propertyObject.address2,
      "unit_address3": req.body.propertyObject.address3,
      "unit_type": req.body.propertyObject.unitType,
      "unit_units": req.body.propertyObject.unit_units,
      "unit_carspaces": req.body.propertyObject.carSpaces,
      "unit_leasedspaces": req.body.propertyObject.unit_leasedspaces,
      "financial_year_start": req.body.propertyObject.financialYearStart,
      "financial_year_end": req.body.propertyObject.financialYearEnd,
      "vat_registration_no": req.body.propertyObject.vatNo,
      "email_address": req.body.propertyObject.email,
      "web_address": req.body.propertyObject.webAddress,
      "financial_management": req.body.propertyObject.financialManagement,
      "direct_debit_details": req.body.propertyObject.direct_debit_details,
      "contractor_payment_details": req.body.propertyObject.contractor_payment_details,
      "landlord_id": req.body.propertyObject.landlord_id
    };

    if (!validateEmail(req.body.propertyObject.email)) {
      res.json({ OK: 0, message: "Please check the information provided" });
    }
    else {
      // var newValues = req.body.propertyObject;
      db.collection("unit").findOneAndUpdate(query, { $set: newValues }, function (err, result) {
        var msg = { message: "" };

        if (result.value === null) {
          msg['message'] = "No property found.";
        } else {

          msg['message'] = "Property updated.";
        }
        res.send(msg);
      });
      client.close();
    }
  });
});

//  RAISE AN ISSUE
router.post('/issues/raise/', function (req, res, next) {

  MongoClient.connect(url, (err, client) => {
    if (err) throw err;
    var db = client.db('finalgroupproject');
    var ticketObj = req.body;
    var unit_ref = new DbRef("unit", new ObjectID(req.body.unit_id));
    var query = { "_id": new ObjectID(req.body.unit_id) };
    // if (!validatePhone(ticketObj.phone)) {
    //   res.json({ OK: 0, message: "Please check the information provided" });
    // }
    // else {
      db.collection("unit").findOne(query, function (err, result) {
        if (err) throw err;
        var landlord_ref = result.landlord_id;

        db.collection("category").findOne({ _id: parseInt(ticketObj.category) }, function (err2, result2) {
          if (err2) throw err2;
          var current_time = Date.now();
          var ticket = {
            category: result2.name,
            dateAvailable: ticketObj.dateAvailable,
            hourAvailable: ticketObj.hourAvailable,
            description: ticketObj.description,
            unitName: result.unit_name,
            landlord_id: landlord_ref,
            phone: ticketObj.phone,
            subject: ticketObj.subject,
            unit_id: unit_ref,
            status: "CREATED",
            priority: result2.priority,
            time_created: current_time,
            jobReports: [],
            status_history: [{
              time: current_time,
              content: "Ticket was created."
            }]
          };

          db.collection("landlord").findOne({ _id: landlord_ref.oid }, function (err3, result3) {
            if (err3) throw err3;

            db.collection("ticket").insert(ticket, function (err1, result1) {
              if (err1) throw err1;

              var content = util.format(new_ticket_email_template, result3.landlord_name, result.unit_name, ticketObj.subject, result2.name, ticketObj.description);
              var subject = util.format('Service Request Raised on Propery %s', result.unit_name)
              var emailObject = {
                to_address: result3.landlord_email,
                subject: subject,
                content
              };

              send_email(emailObject, function(error, info){
                if (error) throw error;
                console.log(info);
                res.send(result1);
              });
            });
          });
        });
      });
    // }
  });
});

// CHANGE TICKET STATUS
router.post('/issue/status/change', function (req, res, next) {

  MongoClient.connect(url, (err, client) => {
    if (err) throw err;
    var db = client.db('finalgroupproject');
    console.log(req.body);
    var status = req.body.status
    var query = { "_id": new ObjectID(req.body.id) };
    var current_time = Date.now();
    var ticket_history = {
      time: current_time
    };
    var push = {
      status_history: ticket_history
    }
    switch (req.body.status) {
      case 'OPEN':
        ticket_history['content'] = 'This ticket was approved.';
        break;

      case 'IN_PROGRESS':
        ticket_history['content'] = 'This ticket was accepted by our assigned contractor, who will get in touch shortly.';
        break;

      case 'REJECTED':
        ticket_history['content'] = 'This ticket was rejected by our assigned contractor. A new contractor will be assigned shortly.';
        status = 'OPEN';
        break;

      case 'FINISHED':
        ticket_history['content'] = 'All work regarding this ticket was finished.';
        ticket_history['jobReport'] = req.body.jobReport;
        push['jobReports'] = req.body.jobReport;
        break;

      case 'REASSIGN':
        ticket_history['content'] = 'Further work required. Our staff will reassign a contractor shortly.';
        status = 'OPEN';
        break;

      case 'CLOSED':
        ticket_history['content'] = 'This ticket was closed.';
        break;

      default:
        break;
    }
    var newValues = { status };

    if (req.body.status == 'FINISHED') {
      newValues['time_finished'] = current_time;
    }

    if (req.body.status == 'CLOSED') {
      newValues['time_closed'] = current_time;
    }

    db.collection("ticket").findOneAndUpdate(query, { $set: newValues, $push: push }, function (err, ticket) {
      if (err) throw err;

      db.collection("landlord").findOne({ _id: new ObjectID(ticket.value.landlord_id.oid) }, function (err2, landlord) {
        if (err2) throw err2;

        var emailObject = {
          to_address: landlord.landlord_email,
          subject: util.format(ticket_status_update_subject_template, ticket.value.subject),
          content: util.format(ticket_status_update_email_template, landlord.landlord_name,ticket.value.subject, ticket_history.content)
        };
        send_email(emailObject, function (err1, info) {
          if (err1) throw err1;
          console.log(info);
          res.send(ticket);
        });
      })
    });
  });
});

// ASSIGN TICKET TO CONTRACTOR
router.post('/issue/assign/', function (req, res, next) {

  MongoClient.connect(url, (err, client) => {
    if (err) throw err;
    var db = client.db('finalgroupproject');
    var t_id = new ObjectID(req.body.ticketId);
    var c_id = new ObjectID(req.body.contractorId);
    var importance = req.body.importance;

    var contractor_ref = {
      importance,
      status: "ASSIGNED",
      contractor_id: new DbRef("contractor", c_id)
    };

    var ticket_ref = {
      ticket_id: new DbRef("ticket", t_id)
    };

    db.collection('contractor').findOne({ _id: c_id }, function (err, contractor) {

      var ticket_history = {
        time: Date.now(),
        content: util.format("This ticket was assigned to our contractor %s (Phone: %s)", contractor.contractor_name, contractor.contractor_phone_work),
        contractor_id: contractor._id,
        contractor_name: contractor.contractor_name
      }

      db.collection("ticket").findOneAndUpdate({ _id: t_id }, { $set: contractor_ref, $push: { status_history: ticket_history } }, function (err1, ticket) {
        if (err1) {
          return console.log("error: " + err1);
        }
        console.log(ticket.value);

        db.collection("landlord").findOne({ _id: new ObjectID(ticket.value.landlord_id.oid) }, function (err2, landlord) {
          if (err2) throw err2;

          var emailObject = {
            to_address: landlord.landlord_email,
            subject: util.format(ticket_status_update_subject_template, ticket.value.subject),
            content: util.format(ticket_status_update_email_template, landlord.landlord_name,ticket.value.subject, ticket_history.content)
          };
          send_email(emailObject, function (err3, info) {
            if (err3) throw err3;
            console.log(info);
          });
        })
      });

      db.collection("contractor").findOneAndUpdate({ _id: c_id }, { $push: ticket_ref }, function (err2, result1) {
        if (err2) {
          return console.log("error: " + err2);
        }
        console.log(result1)
        var emailObject = {
          to_address: result1.value.contractor_email,
          subject: "New Ticket Assigned To You",
          content: util.format(new_ticket_assigned_email_template, result1.value.contractor_name)
        };
        send_email(emailObject, function (err3, info) {
          if (err3) throw err3;
          console.log(info);
        });
        res.send(result1);
      });
    });
  });
});

// GET RAISED TICKETS BY ID
router.post('/issue', function (req, res, next) {

  MongoClient.connect(url, (err, client) => {
    if (err) throw err;
    var db = client.db('finalgroupproject');
    var query = { "_id": new ObjectID(req.body.id) };
    db.collection("ticket").findOne(query, function (err, result) {
      if (err) throw err;
      res.send(result);
    });
    client.close();
  });
});

// FILTER TICKETS BY TICKET STATUS
router.post('/issues/filter/status', function (req, res, next) {
  var status = req.body.status;

  MongoClient.connect(url, (err, client) => {
    if (err) throw err;
    var db = client.db('finalgroupproject');
    var query = { "status": status };
    db.collection("ticket").find(query).sort(getNatrulSortOrder('ticket', 1)).toArray(function (err, result) {
      res.send(result);
    });
    client.close();
  });
});


// REMOVE A TICKET BY ID
router.post('/issues/remove/', function (req, res, next) {

  MongoClient.connect(url, (err, client) => {
    if (err) throw err;
    var db = client.db('finalgroupproject');
    var query = { "_id": new ObjectID(req.body.temp) };

    db.collection("ticket").findOneAndDelete(query, function (err, result) {
      console.log(result);

      var msg = { message: "" };
      if (result.value === null) {
        msg['message'] = "No ticket found.";
      } else {

        msg['message'] = "Ticket removed.";
      }
      res.send(msg);
    });
    client.close();
  });
});

// UPDATE A ISSUE
router.post('/issues/update/', function (req, res, next) {

  MongoClient.connect(url, (err, client) => {
    if (err) throw err;
    var db = client.db('finalgroupproject');
    var query = { "_id": new ObjectID(req.body.id) };
    var newValues = req.body.ticketObject;

    if (!validatePhone(req.body.ticketObject.contactNumber)) {
      res.json({ OK: 0, message: "Please check the information provided" });
    }

    else {
      db.collection("ticket").findOneAndUpdate(query, { $set: newValues }, function (err, result) {
        var msg = { message: "" };

        if (result.value === null) {
          msg['message'] = "No ticket found.";
        } else {

          msg['message'] = "Ticket updated.";
        }
        res.send(msg);
      });
      client.close();
    }
  });
});

// GET ALL LANDLORDS
router.get('/landlords', function (req, res, next) {
  console.log(req.session.username);
  MongoClient.connect(url, (err, client) => {
    if (err) throw err;
    var db = client.db('finalgroupproject');
    db.collection("landlord").find().sort(getNatrulSortOrder('landlord', 1)).toArray(function (err, result) {
      if (err) throw err;
      res.send(result);
    });
    client.close();
  });
});

// GET A LANDLORD DETAIL BY ID
router.post('/landlord', function (req, res, next) {

  MongoClient.connect(url, (err, client) => {
    if (err) throw err;
    var db = client.db('finalgroupproject');
    var query = { "_id": new ObjectID(req.body.id) };
    db.collection("landlord").findOne(query, function (err, result) {
      if (err) throw err;
      res.send(result);
    });
    client.close();
  });
});

// UPDATE A LANDLORD BY ID
router.post('/landlord/update/', function (req, res, next) {

  MongoClient.connect(url, (err, client) => {
    if (err) throw err;
    var db = client.db('finalgroupproject');
    var query = { "_id": new ObjectID(req.body.id) };

    var newValues = {
      "landlord_name": req.body.landlordObject.landlordName,
      "landlord_phone_work": req.body.landlordObject.landlordPhoneWork,
      "landlord_phone_home": req.body.landlordObject.landlordPhoneHome,
      "landlord_phone_mobile": req.body.landlordObject.landlordPhoneMobile,
      "landlord_email": req.body.landlordObject.email,
      "landlord_address": req.body.landlordObject.password,
      "login_code": req.body.landlordObject.unevailable_period,
      "bank_name": req.body.landlordObject.bankName,
      "bank_address": req.body.landlordObject.bankAddress,
      "bank_BIC": req.body.landlordObject.bic,
      "bank_IBAN": req.body.landlordObject.iban,
      "bank_phone": req.body.landlordObject.phoneBank,
      "bank_email": req.body.landlordObject.emailBank,
      "bank_fax": req.body.landlordObject.fax
    };

    if (!validatePhone(req.body.landlordObject.landlordPhoneWork) || !validateEmail(req.body.landlordObject.email) || req.body.landlordObject.password.length <= 5) {
      res.json({ OK: 0, message: "Please check the information provided" });
    }

    else {
      db.collection("landlord").findOneAndUpdate(query, { $set: newValues }, function (err, result) {
        var msg = { message: "" };

        if (result.value === null) {
          msg['message'] = "No landlord found.";
        } else {

          msg['message'] = "Landlord updated.";
        }
        res.send(msg);
      });
      client.close();
    }
  });
});

// REMOVE A LANDLORD BY ID
router.post('/landlords/remove/', function (req, res, next) {

  MongoClient.connect(url, (err, client) => {
    if (err) throw err;
    var db = client.db('finalgroupproject');
    var query = { "_id": new ObjectID(req.body.id) };
    db.collection("landlord").findOneAndDelete(query, function (err, result) {
      var msg = { message: "" };
      if (result.value === null) {
        msg['message'] = "No landlord found.";
      } else {

        msg['message'] = "Landlord removed.";
      }
      res.send(msg);
    });
    client.close();
  });
});

router.post('/logout', function (req, res) {
  sess = req.session;
  sess.destroy();
  res.send({});
});

// SEARCH DOCUMENT IN DB
router.post('/search/', function (req, res, next) {
  MongoClient.connect(url, (err, client) => {
    if (err) throw err;
    var db = client.db('finalgroupproject');
    var collection = req.body.collection;
    var field = req.body.field;
    var value = req.body.value;
    var query = {};

    if (field == "landlord_name" && collection == 'unit') {
      db.collection('landlord').findOne({ landlord_name: new RegExp(value, 'i') }, function (err, landlord) {
        if (err) {
          return console.log("error: " + err);
        }
        var ref = new DbRef("landlord", new ObjectID(landlord._id));
        query = {
          landlord_id: ref
        };
        db.collection(collection).find(query).toArray(function (err, result) {
          res.send(result);
        });
      });
    } else {
      if (field == "address" && collection == 'unit') {
        var ors = [];
        for (i = 1; i <= 3; i++) {
          ors[i - 1] = {};
          ors[i - 1]['unit_address' + i.toString()] = new RegExp(value, 'i');
        }
        query['$or'] = ors;
      }
      else {
        query[field] = new RegExp(value, 'i');
      }

      db.collection(collection).find(query).toArray(function (err, result) {
        res.send(result);
      });
    }
  });
});

function getNatrulSortOrder(collection, ascd) {
  ascd = ascd == 1 ? 1 : -1;
  switch (collection) {
    case "landlord":
      return { landlord_name: ascd };
    case "unit":
      return { unit_name: ascd };
    case "contractor":
      return { unit_name: ascd };
    case "ticket":
      return { subject: ascd };
    case "category":
      return { name: ascd };
    default:
      break;
  }
}

const DbRef = require('mongodb').DBRef;
// Get Tenant By property id
router.get('/tenant/:id/', function (req, res, next) {
  MongoClient.connect(url, (err, client) => {
    if (err) throw err;
    var db = client.db('finalgroupproject');
    var ref = new DbRef("unit", new ObjectID(req.params.id));
    var query = {
      unit_id: ref
    };

    db.collection("leaseholder").find(query).toArray(function (err, result) {

      res.send(result)
    });
    client.close();
  });
});

// FETCH ALL PROPERTIES ASSOCIATED WITH A LANDLORD USING LANDLORD ID
router.post('/landlord/property', function (req, res, next) {
  MongoClient.connect(url, (err, client) => {
    if (err) throw err;
    var db = client.db('finalgroupproject');

    var ref = new DbRef("landlord", new ObjectID(req.body.id));
    var query = {
      landlord_id: ref
    };

    db.collection("unit").find(query).toArray(function (err, result) {

      res.send(result)
    });
    client.close();
  });
});

// FETCH ALL TICKETS ASSOCIATED WITH A LANDLORD USING LANDLORD ID
router.post('/landlord/ticket', function (req, res, next) {
  MongoClient.connect(url, (err, client) => {
    if (err) throw err;
    var db = client.db('finalgroupproject');
    var ref = new DbRef("landlord", new ObjectID(req.body.id));
    var query = {
      landlord_id: ref
    };

    db.collection("ticket").find(query).toArray(function (err, result) {

      res.send(result)
    });
    client.close();
  });
});

// FETCH ALL TICKETS ASSOCIATED WITH A PROPERTY USING PROPERTY ID
router.post('/unit/ticket/', function (req, res, next) {
  MongoClient.connect(url, (err, client) => {
    if (err) throw err;
    var db = client.db('finalgroupproject');
    var ref = new DbRef("unit", new ObjectID(req.body.id));
    var query = {
      unit_id: ref
    };

    db.collection("ticket").find(query).toArray(function (err, result) {

      res.send(result)
    });
    client.close();
  });
});

// FETCH ALL TICKETS ASSOCIATED WITH A CONTRACTOR USING CONTRACTOR ID
router.post('/contractor/ticket/', function (req, res, next) {
  MongoClient.connect(url, (err, client) => {
    if (err) throw err;
    var db = client.db('finalgroupproject');
    var ref = new DbRef("contractor", new ObjectID(req.body.id));
    var query = {
      contractor_id: ref
    };

    db.collection("ticket").find(query).sort(getNatrulSortOrder("ticket",1)).toArray(function (err, result) {

      res.send(result)
    });
    client.close();
  });
});

// FETCH ALL ISSUE CATEGORIES
router.post('/category', function (req, res, next) {

  MongoClient.connect(url, (err, client) => {
    if (err) throw err;
    var db = client.db('finalgroupproject');
    db.collection("category").find().sort(getNatrulSortOrder('category', 1)).toArray(function (err, result) {

      res.send(result);
    });
    client.close();
  });
});

// GET ALL CONTRACTORS
router.get('/contractors', function (req, res, next) {
  console.log(req.session.username);
  MongoClient.connect(url, (err, client) => {
    if (err) throw err;
    var db = client.db('finalgroupproject');
    db.collection("contractor").find().sort(getNatrulSortOrder('contractor', 1)).toArray(function (err, result) {
      if (err) throw err;
      res.send(result);
    });
    client.close();
  });
});

// GET A CONTRACTOR DETAIL BY ID
router.post('/contractor', function (req, res, next) {

  MongoClient.connect(url, (err, client) => {
    if (err) throw err;
    var db = client.db('finalgroupproject');
    var query = { "_id": new ObjectID(req.body.id) };
    db.collection("contractor").findOne(query, function (err, result) {
      if (err) throw err;
      res.send(result);
    });
    client.close();
  });
});

// REMOVE A CONTRACTOR BY ID
router.post('/contractors/remove/', function (req, res, next) {

  MongoClient.connect(url, (err, client) => {
    if (err) throw err;
    var db = client.db('finalgroupproject');
    var query = { "_id": new ObjectID(req.body.id) };
    db.collection("contractor").findOneAndDelete(query, function (err, result) {
      var msg = { message: "" };
      if (result.value === null) {
        msg['message'] = "No contractor found.";
      } else {

        msg['message'] = "Contractor removed.";
      }
      res.send(msg);
    });
    client.close();
  });
});

// UPDATE A CONTRACTOR
router.post('/contractors/update/', function (req, res, next) {

  MongoClient.connect(url, (err, client) => {
    if (err) throw err;
    var db = client.db('finalgroupproject');
    var query = { "_id": new ObjectID(req.body.id) };
    var newValues = req.body.contractorObject;

    if (!validatePhone(req.body.contractorObject.contractor_phone_work) ||
      !validatePhone(req.body.contractorObject.contractor_phone_mobile) ||
      !validatePhone(req.body.contractorObject.contractor_phone_home) ||
      !validateEmail(req.body.contractorObject.contractor_email)
    ) {
      res.json({ OK: 0, message: "Please check the information provided" });
    }

    else {
      db.collection("contractor").findOneAndUpdate(query, { $set: newValues }, function (err, result) {
        var msg = { message: "" };

        if (result.value === null) {
          msg['message'] = "No contractor found.";
        } else {

          msg['message'] = "Contractor updated.";
        }
        res.send(msg);
      });
      client.close();
    }
  });
});

//Add a Contractor
router.post('/addContractor', function (req, res, next) {
  // gets form information
  MongoClient.connect(url, (err, client) => {
    if (err) throw err;
    var contractorObject = req.body.contractorObject;

    // if (!validatePhone(contractorObject.contractor_phone_work) ||
    //   !validatePhone(contractorObject.contractor_phone_mobile) ||
    //   !validatePhone(contractorObject.contractor_phone_home) ||
    //   !validateEmail(contractorObject.contractor_email)) {
    //   res.json({ OK: 0, message: "Please check the information provided" });
    // }

    // else {
      var query = {
        "contractor_name": contractorObject.contractor_name,
        "contractor_phone_home": contractorObject.contractor_phone_home,
        "contractor_phone_mobile": contractorObject.contractor_phone_mobile,
        "contractor_phone_work": contractorObject.contractor_phone_work,
        "contractor_email": contractorObject.contractor_email,
        "contractor_address": contractorObject.contractor_address,
        user_type: 4,
        "password": password_generator.generate({
          length: 6,
          numbers: true
        }),
        ticket_id: []


      };
      var db = client.db('finalgroupproject');
      db.collection("contractor").insert(query, function (err, result) {
        var msg = { message: null };

        if (result.value === null) {
          msg = { message: "Contractor details not added" };
        } else {
          res.send(result);
        };
      });
    // }
  });
});

function send_email(emailObject, callback) {
  const from_address = 'fitix.skrrrrrrr@gmail.com';
  const from_address_pass = "Iam,123.";
  var transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
      user: from_address,
      pass: from_address_pass
    }
  });

  var mailOptions = {
    from: from_address,
    to: emailObject.to_address,
    subject: emailObject.subject,
    text: emailObject.content
  };

  transporter.sendMail(mailOptions, function (error, info) {

    callback(error, info);
  });
};

// router.post('/imageUpload/', upload.single('file'), function (req, res, next) {
//   console.log("inside")
// });



module.exports = router;
