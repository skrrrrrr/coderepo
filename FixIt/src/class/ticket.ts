export class Ticket {
    private id: String;
    private subject: String;
    private unitName: String;
    private contactNumber: String;
    private dateAvailable: String;
    private hourAvailable: String;
    private category: String;
    private subTask: String;
    private description: String;

    // constructor(id, name, unitName, contactNumber, dateAvailable, hourAvailable, category, subTask, description) {
    //     this.id = id;
    //     this.name = name;
    //     this.unitName = unitName;
    //     this.contactNumber = contactNumber;
    //     this.dateAvailable = dateAvailable;
    //     this.hourAvailable = hourAvailable;
    //     this.category = category;
    //     this.subTask = subTask;
    //     this.description = description;
    // }
    constructor(){
        
    }
    setId(id){
        this.id = id;
    }

    setSubject(subject){
        this.subject = subject;
    }

    setUnitName(unitName){
        this.unitName = unitName;
    }

    setContactNumber(contactNumber){
        this.contactNumber = contactNumber;
    }

    setDateAvailable(dateAvailable){
        this.dateAvailable = dateAvailable;
    }

    setHourAvailable(hourAvailable){
        this.hourAvailable = hourAvailable;
    }

    setCategory(category){
        this.category = category;
    }

    setSubTask(subTask){
        this.subTask = subTask;
    }

    setDescription(description){
        this.description = description;
    }
}