import { Component, OnInit } from '@angular/core';
import { NgForm, FormControl, FormGroup } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { UserTypeService } from '../user-type.service';
import { toInteger } from '@ng-bootstrap/ng-bootstrap/util/util';

@Component({
  selector: 'app-assign-contractor',
  templateUrl: './assign-contractor.component.html',
  styleUrls: ['./assign-contractor.component.css']
})
export class AssignContractorComponent implements OnInit {

  id: string;
  userRole: number;
  contractors;
  ticketId: string;
  private sub: any;

  assignForm = new FormGroup({
    assignContractor: new FormControl(),
    importanceLevel: new FormControl(),
  });

  constructor(private httpClient: HttpClient, private route: ActivatedRoute, private router2: Router, private userTypeService:UserTypeService) { }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      this.ticketId = params['id'];
    });
    this.id = this.userTypeService.getUserId();
    this.userRole = this.userTypeService.getRole();
    this.getAllContractor();
  }

  getAllContractor(){
    return this.httpClient.get<String>('http://localhost:4600/api/contractors', {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    })
      .subscribe(
        (data: any) => {
          console.log(data);
          this.contractors = data;
        })
  }

  onSubmit(){
    var contractorId = this.assignForm.value.assignContractor;
    var importance = parseInt(this.assignForm.value.importanceLevel);
    var ticketId = this.ticketId;
    var assignObject = { contractorId, importance, ticketId };
    var jsonObject = JSON.parse(JSON.stringify(assignObject));
    console.log(importance);
    
    this.httpClient.post<String>('http://localhost:4600/api/issue/assign/', jsonObject, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      }),
      withCredentials: true
    })
      .subscribe(
        (data: any) => {
          console.log(data);
          this.router2.navigateByUrl('/tickets');
        })
  }

}
