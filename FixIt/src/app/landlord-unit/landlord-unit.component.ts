import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { UserTypeService } from '../user-type.service';
import { FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-landlord-unit',
  templateUrl: './landlord-unit.component.html',
  styleUrls: ['./landlord-unit.component.css']
})
export class LandlordUnitComponent implements OnInit {

  unitsByLandlordId: any;
  id: string;
  userRole: number;
  landlordId: string;
  private sub: any;

  constructor(private router: ActivatedRoute, private httpClient: HttpClient, private router2: Router, private userTypeService: UserTypeService) { }

  ngOnInit() {
    this.sub = this.router.params.subscribe(params => {
      this.landlordId = params['id'];
    });
    this.id = this.userTypeService.getUserId();
    this.userRole = this.userTypeService.getRole();
    this.getData();
  }

  getData() {
      var id = this.landlordId;

      return this.httpClient.post<String>('http://localhost:4600/api/landlord/property', JSON.stringify({ id }), {
        headers: new HttpHeaders({
          'Content-Type': 'application/json'
        })
      })
        .subscribe(
          (data: any) => {
            console.log(data);
            this.unitsByLandlordId = data;
          });
    
  }
}
