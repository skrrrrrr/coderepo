import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LandlordUnitComponent } from './landlord-unit.component';

describe('LandlordUnitComponent', () => {
  let component: LandlordUnitComponent;
  let fixture: ComponentFixture<LandlordUnitComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LandlordUnitComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LandlordUnitComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
