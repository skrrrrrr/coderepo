import { Component, OnInit } from '@angular/core';
import { NgForm, FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { debounceTime } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { UserTypeService } from '../user-type.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginForm = new FormGroup({
    type: new FormControl(),
    email: new FormControl(),
    password: new FormControl(),
  });
  private _success = new Subject<string>();
  staticAlertClosed = false;
  successMessage: string;

  ngOnInit() {
    setTimeout(() => this.staticAlertClosed = true, 20000);

    this._success.subscribe((message) => this.successMessage = message);
    this._success.pipe(
      debounceTime(10000)
    ).subscribe(() => this.successMessage = null);
  }

  constructor(private router: Router, private httpClient: HttpClient, private userTypeService: UserTypeService) { }
  onSubmit() {
    //var email = this.loginForm.value.email;
    //this.router.navigate(['admin-home']);

    var type = this.loginForm.value.type;
    var email = this.loginForm.value.email;
    var password = this.loginForm.value.password;
    var loginObject = { type, email, password };
    //if statment begins validation checks
    var jsonObject = JSON.parse(JSON.stringify(loginObject));
    this.httpClient.post<String>('http://localhost:4600/api/login', jsonObject, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      }),
      withCredentials: true
    })
      .subscribe(
        (data: any) => {
          //save user role to service
          this.userTypeService.setRole(type);
          this.userTypeService.setUserId(data.id);
          console.log(data.id);
          
          //check if user or admin (user role)
          if (data.length != 0 && data.success == 1) {
            if(data.type == 0){

            } else if(data.type == 1){
              this.router.navigate(['admin-home']);
            } else if(data.type == 2){
              this.router.navigate(['properties'])
            } else if (data.type == 4){
              this.router.navigate(['contractor-home'])

            } else {

            }
          }
          else {
            this.displayNotification();
          }
        })
  }

  public displayNotification() {
    this._success.next('Username or/and Password incorrect');
  }
}
