import { Injectable, OnInit } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UserTypeService {

  userRole: number;
  userId: string;
  constructor() {
    if (localStorage.getItem("userRole") != "") {
      this.userRole = Number(localStorage.getItem("userRole"));
    } else {
      this.userRole = -1;
    }
    if (localStorage.getItem("userId") != "") {
      this.userId = localStorage.getItem("userId");
    } else {
      this.userId = "NULL";
    }
  }
  getRole() {
    console.log(this.userRole);

    return this.userRole;
  }
  setRole(role: number) {
    localStorage.setItem("userRole", "" + role);
    this.userRole = role;
  }
  getUserId() {
    return this.userId;
  }
  setUserId(userid: string) {
    localStorage.setItem("userId", userid);

    this.userId = userid;
  }
  clear() {
    localStorage.setItem("userRole", "");
    localStorage.setItem("userId", "");

    this.userRole = -1;
    this.userId = "NULL";
  }
}
