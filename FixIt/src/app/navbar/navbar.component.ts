import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { UserTypeService } from '../user-type.service';
import { typeWithParameters } from '@angular/compiler/src/render3/util';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  userRole:number;

  constructor(private router: Router, private httpClient:HttpClient, private userTypeService: UserTypeService) {
    this.userRole = this.userTypeService.getRole();
   }

  ngOnInit() {
    this.userRole = this.userTypeService.getRole();
    // console.log("user role "+ this.userRole);
  }

  logout() {
    var loginObject = {};
    this.userTypeService.clear();
    var jsonObject = JSON.parse(JSON.stringify(loginObject));
    this.httpClient.post<String>('http://localhost:4600/api/logout', jsonObject, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    })
      .subscribe(
        (data: any) => {
          this.router.navigateByUrl('');
        })
 }

}
