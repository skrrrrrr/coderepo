import { Component, OnInit } from '@angular/core';
import { NgForm, FormControl, FormGroup } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { debounceTime } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Component({
	selector: 'app-edit-landlord',
	templateUrl: './edit-landlord.component.html',
	styleUrls: ['./edit-landlord.component.css']
})
export class EditLandlordComponent implements OnInit {

	editLandlordForm = new FormGroup({
		landlordName: new FormControl(),
		landlordPhoneWork: new FormControl(),
		landlordPhoneHome: new FormControl(),
		landlordPhoneMobile: new FormControl(),
		email: new FormControl(),
		password: new FormControl(),
		bankName: new FormControl(),
		bankAddress: new FormControl(),
		bic: new FormControl(),
		iban: new FormControl(),
		phoneBank: new FormControl(),
		emailBank: new FormControl(),
		fax: new FormControl(),
	});

	id: number;
	private sub: any;
	landlordById: any;
	private _success = new Subject<string>();
	staticAlertClosed = false;
	successMessage: string;

	constructor(private router: ActivatedRoute, private httpClient: HttpClient, private router2: Router) { }

	ngOnInit() {
		this.sub = this.router.params.subscribe(params => {
			this.id = params['id'];
		});
		this.getData();
		setTimeout(() => this.staticAlertClosed = true, 20000);

		this._success.subscribe((message) => this.successMessage = message);
		this._success.pipe(
			debounceTime(10000)
		).subscribe(() => this.successMessage = null);
	}

	getData() {
		var id = this.id;
		return this.httpClient.post<String>('http://localhost:4600/api/landlord', JSON.stringify({ id }), {
			headers: new HttpHeaders({
				'Content-Type': 'application/json'
			}),
	  		withCredentials:true
		})
			.subscribe(
				(data: any) => {
					console.log(data);
					this.landlordById = data;

					this.editLandlordForm.patchValue({
						id: data._id,
						landlordName: data.landlord_name,
						landlordPhoneWork: data.landlord_phone_work,
						landlordPhoneHome: data.landlord_phone_home,
						landlordPhoneMobile: data.landlord_phone_mobile,
						email: data.landlord_email,
						password: data.password,
						bankName: data.bank_name,
						bankAddress: data.bank_address,
						bic: data.bank_BIC,
						iban: data.bank_IBAN,
						phoneBank: data.bank_phone,
						emailBank: data.bank_email,
						fax: data.bank_fax
					});
				})
	}

	onSubmit() {
		//var email = this.loginForm.value.email;
		//this.router.navigate(['admin-home']);
		var id = this.id;
		console.log(id);
		var landlordName = this.editLandlordForm.value.landlordName;
		var landlordPhoneWork = this.editLandlordForm.value.landlordPhoneWork;
		var landlordPhoneHome = this.editLandlordForm.value.landlordPhoneHome;
		var landlordPhomeMobile = this.editLandlordForm.value.landlordPhoneMobile;
		var email = this.editLandlordForm.value.email;
		var password = this.editLandlordForm.value.password;
		var bankName = this.editLandlordForm.value.bankName;
		var bankAddress = this.editLandlordForm.value.bankAddress;
		var bic = this.editLandlordForm.value.bic;
		var iban = this.editLandlordForm.value.iban;
		var phoneBank = this.editLandlordForm.value.phoneBank;
		var emailBank = this.editLandlordForm.value.emailBank;
		var fax = this.editLandlordForm.value.fax;
		var landlordObject = { landlordName, landlordPhoneWork, landlordPhoneHome, landlordPhomeMobile, email, password, emailBank, bankName, bankAddress, bic, iban, phoneBank, fax };
		var jsonObject = JSON.parse(JSON.stringify({ id, landlordObject }));
		this.httpClient.post<String>('http://localhost:4600/api/landlord/update/', jsonObject, {
			headers: new HttpHeaders({
				'Content-Type': 'application/json'
			}),
	  		withCredentials:true
		})
			.subscribe(
				(data: any) => {
					this.displayNotification();
					console.log(data);
				});
	}

	public displayNotification() {
		this._success.next('Edit Success');
	}
}


