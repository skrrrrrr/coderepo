import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewPropertyTicketsComponent } from './view-property-tickets.component';

describe('ViewPropertyTicketsComponent', () => {
  let component: ViewPropertyTicketsComponent;
  let fixture: ComponentFixture<ViewPropertyTicketsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewPropertyTicketsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewPropertyTicketsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
