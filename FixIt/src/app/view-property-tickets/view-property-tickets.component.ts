import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { UserTypeService } from '../user-type.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-view-property-tickets',
  templateUrl: './view-property-tickets.component.html',
  styleUrls: ['./view-property-tickets.component.css']
})
export class ViewPropertyTicketsComponent implements OnInit {

  ticketsByPropertyId: any;
  propertyId: string;
  private sub: any;

  constructor(private httpClient: HttpClient, private router: ActivatedRoute, private userTypeService: UserTypeService) { 
    
  }

  ngOnInit() {
    this.sub = this.router.params.subscribe(params => {
      this.propertyId = params['id'];
      this.getData();
    });
  }

  getData(){
    var id = this.propertyId;
      console.log("here"+id);

      return this.httpClient.post<String>('http://localhost:4600/api/unit/ticket/', JSON.stringify({ id }), {
        headers: new HttpHeaders({
          'Content-Type': 'application/json'
        })
      })
        .subscribe(
          (data: any) => {
            console.log(data);
            this.ticketsByPropertyId = data;
          });
  }
}
