import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule, Routes } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap' ;

import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { AdminHomeComponent } from './admin-home/admin-home.component';
import { LandlordFormComponent } from './landlord-form/landlord-form.component';
import { PropertyFormComponent } from './property-form/property-form.component';
import { TicketFormComponent } from './ticket-form/ticket-form.component';
import { ViewPropertyComponent } from './view-property/view-property.component';
import { EditPropertyComponent } from './edit-property/edit-property.component';
import { ViewLandlordsComponent } from './view-landlords/view-landlords.component';
import { EditLandlordComponent } from './edit-landlord/edit-landlord.component';
import { ViewTicketsComponent } from './view-tickets/view-tickets.component';
import { EditTicketComponent } from './edit-ticket/edit-ticket.component';
import { NavbarComponent } from './navbar/navbar.component';
import { FooterComponent } from './footer/footer.component';
import { ViewContractorsComponent } from './view-contractors/view-contractors.component';
import { LandlordUnitComponent } from './landlord-unit/landlord-unit.component';
import { EditContractorComponent } from './edit-contractor/edit-contractor.component';
import { ContractorFormComponent } from './contractor-form/contractor-form.component';
import { ViewOwnTicketsComponent } from './view-own-tickets/view-own-tickets.component';
import { ContractorHomeComponent } from './contractor-home/contractor-home.component';
import { ViewPropertyTicketsComponent } from './view-property-tickets/view-property-tickets.component';
import { ViewTicketDetailsComponent } from './view-ticket-details/view-ticket-details.component';
import { AssignContractorComponent } from './assign-contractor/assign-contractor.component';
import { LandlordProfileComponent } from './landlord-profile/landlord-profile.component';
import { YourProfileComponent } from './your-profile/your-profile.component';
import { JobReportFormComponent } from './job-report-form/job-report-form.component';
// import { FileSelectDirective } from 'ng2-file-upload';


const appRoutes: Routes = [
  {path:'contractor-home', component: ContractorHomeComponent},
  {path:'job-report-form/:id', component: JobReportFormComponent},
  {path:'assign-contractor/:id', component: AssignContractorComponent},
  {path:'property-tickets/:id', component: ViewPropertyTicketsComponent},
  {path:'ticket-details/:id', component: ViewTicketDetailsComponent},
  {path:'your-profile', component: YourProfileComponent},
  {path:'your-tickets', component: ViewOwnTicketsComponent},
  {path:'landlord-unit/:id', component: LandlordUnitComponent},
  {path:'landlord-profile/:id', component: LandlordProfileComponent},
  {path:'admin-home', component: AdminHomeComponent},
  {path:'landlord-form', component: LandlordFormComponent},
  {path:'property-form', component: PropertyFormComponent},
  {path:'ticket-form', component: TicketFormComponent},
  {path:'properties', component: ViewPropertyComponent},
  {path:'edit-property/:id', component: EditPropertyComponent},
  {path:'landlords', component: ViewLandlordsComponent},
  {path:'edit-landlord/:id', component: EditLandlordComponent},
  {path:'edit-ticket/:id', component: EditTicketComponent},
  {path:'tickets', component: ViewTicketsComponent},
  {path:'contractor-form', component: ContractorFormComponent},
  {path:'contractors', component: ViewContractorsComponent},
  {path:'edit-contractor/:id', component: EditContractorComponent},
  {path:'', component: LoginComponent}
]

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    AdminHomeComponent,
    LandlordFormComponent,
    PropertyFormComponent,
    TicketFormComponent,
    ViewPropertyComponent,
    EditPropertyComponent,
    ViewLandlordsComponent,
    EditLandlordComponent,
    ViewTicketsComponent,
    EditTicketComponent,
    NavbarComponent,
    FooterComponent,
    LandlordUnitComponent,
    ViewContractorsComponent,
    EditContractorComponent,
    ContractorFormComponent,
    ViewOwnTicketsComponent,
    ContractorHomeComponent,
    ViewPropertyTicketsComponent,
    ViewTicketDetailsComponent,
    AssignContractorComponent,
    LandlordProfileComponent,
    YourProfileComponent,
    JobReportFormComponent,
	// FileSelectDirective
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    RouterModule.forRoot(appRoutes),
	NgbModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
