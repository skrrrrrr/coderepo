import { Component, OnInit } from '@angular/core';
import { NgForm, FormControl, FormGroup } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Subject } from 'rxjs';
import { debounceTime } from 'rxjs/operators';

@Component({
  selector: 'app-edit-property',
  templateUrl: './edit-property.component.html',
  styleUrls: ['./edit-property.component.css']
})

export class EditPropertyComponent implements OnInit {

  editPropertyForm = new FormGroup({
    unitName: new FormControl(),
    address1: new FormControl(),
    address2: new FormControl(),
    address3: new FormControl(),
    unitType: new FormControl(),
    carSpaces: new FormControl(),
    leasedSpaces: new FormControl(),
    financialYearStart: new FormControl(),
    financialYearEnd: new FormControl(),
    vatNo: new FormControl(),
    email: new FormControl(),
    webAddress: new FormControl(),
    financialManagement: new FormControl()
  });

  id: number;
  private sub: any;
  unitById: any;
  private _success = new Subject<string>();
  staticAlertClosed = false;
  successMessage: string;

  constructor(private router: ActivatedRoute, private httpClient: HttpClient, private router2: Router) { }





  ngOnInit() {
    this.sub = this.router.params.subscribe(params => {
      this.id = params['id'];
    });
    this.getData();

    this._success.subscribe((message) => this.successMessage = message);
    this._success.pipe(
      debounceTime(10000)
    ).subscribe(() => this.successMessage = null);
  }


  //Get property info by id from api
  getData() {
    var id = this.id;
    return this.httpClient.post<String>('http://localhost:4600/api/unit/', JSON.stringify({ id }), {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      }),
	  withCredentials:true
    })
      .subscribe(
        (data: any) => {
          console.log(data);
          this.unitById = data;

          this.editPropertyForm.patchValue({
            id: data._id,
            unitName: data.unit_name,
            address1: data.unit_address1,
            address2: data.unit_address2,
            address3: data.unit_address3,
            unitType: data.unit_type,
            carSpaces: data.unit_carspaces,
            leasedSpaces: data.unit_leasedspaces,
            financialYearStart: data.financial_year_start,
            financialYearEnd: data.financial_year_end,
            vatNo: data.vat_registration_no,
            email: data.email_address,
            webAddress: data.web_address,
            financialManagement: data.financial_management
          });
        })
  }

  onSubmit() {
    var id = this.id;
    console.log(id);
    var unitName = this.editPropertyForm.value.unitName;
    var address1 = this.editPropertyForm.value.address1;
    var address2 = this.editPropertyForm.value.address2;
    var address3 = this.editPropertyForm.value.address3;
    var unitType = this.editPropertyForm.value.unitType;
    var carSpaces = this.editPropertyForm.value.carSpaces;
    var leasedSpaces = this.editPropertyForm.value.leasedSpaces;
    var financialYearStart = this.editPropertyForm.value.financialYearStart;
    var financialYearEnd = this.editPropertyForm.value.financialYearEnd;
    var vatNo = this.editPropertyForm.value.vatNo;
    var email = this.editPropertyForm.value.email;
    var webAddress = this.editPropertyForm.value.webAddress;
    var financialManagement = this.editPropertyForm.value.financialManagement;
    var propertyObject = { unitName, address1, address2, address3, unitType, carSpaces, leasedSpaces, financialYearStart, financialYearEnd, vatNo, email, webAddress, financialManagement };
    var jsonObject = JSON.parse(JSON.stringify({ id, propertyObject }));
    this.httpClient.post<String>('http://localhost:4600/api/properties/update/', jsonObject, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      }),
	  withCredentials:true
    })
      .subscribe(
        (data: any) => {
          console.log(data);
          this.getData();
          this.showNotification();
        });
  }

  showNotification() {
    this._success.next('Edit Successful');
  }



}