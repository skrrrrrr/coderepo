import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';
import { UserTypeService } from '../user-type.service';

@Component({
  selector: 'app-your-profile',
  templateUrl: './your-profile.component.html',
  styleUrls: ['./your-profile.component.css']
})
export class YourProfileComponent implements OnInit {

  private sub: any;
  id: string;
  userRole: number;
  landlordDetails: any;

  constructor(private httpClient: HttpClient, private route: ActivatedRoute, private router2: Router, private userTypeService: UserTypeService) { }

  ngOnInit() {
    this.id = this.userTypeService.getUserId();
    this.userRole = this.userTypeService.getRole();
    this.getData();
  }

  getData() {
    var id = this.id;
    return this.httpClient.post<String>('http://localhost:4600/api/landlord', JSON.stringify({ id }), {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    })
      .subscribe(
        (data: any) => {
          console.log(data);
          this.landlordDetails = data;
          console.log(this.landlordDetails);
        })
  }

}
