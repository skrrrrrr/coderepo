import { Component, OnInit } from '@angular/core';
import { NgForm, FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { UserTypeService } from '../user-type.service';

@Component({
  selector: 'app-landlord-form',
  templateUrl: './landlord-form.component.html',
  styleUrls: ['./landlord-form.component.css']
})
export class LandlordFormComponent {

  id: string;
  userRole: number;

  landlordForm = new FormGroup({
    landlordName: new FormControl(),
    landlordPhoneWork: new FormControl(),
    landlordPhoneHome: new FormControl(),
    landlordPhoneMobile: new FormControl(),
    email: new FormControl(),
    landlordAddress: new FormControl(),
    password: new FormControl(),
    bankName: new FormControl(),
    bankAddress: new FormControl(),
    bic: new FormControl(),
    iban: new FormControl(),
    phoneBank: new FormControl(),
    emailBank: new FormControl(),
    fax: new FormControl()
  });

  constructor(private router: Router, private httpClient: HttpClient, private userTypeService: UserTypeService) { }

  ngOnInit(){
    this.id = this.userTypeService.getUserId();
    this.userRole = this.userTypeService.getRole();
  }

  //doesn't have landlord API

  onSubmit() {
    //var email = this.loginForm.value.email;
    //this.router.navigate(['admin-home']);

    var landlordName = this.landlordForm.value.landlordName;
    var landlordPhoneWork = this.landlordForm.value.landlordPhoneWork;
    var landlordPhoneHome = this.landlordForm.value.landlordPhoneHome;
    var landlordPhoneMobile = this.landlordForm.value.landlordPhoneMobile;
    var email = this.landlordForm.value.email;
    var landlordAddress = this.landlordForm.value.landlordAddress;
    var bankName = this.landlordForm.value.bankName;
    var bankAddress = this.landlordForm.value.bankAddress;
    var bic = this.landlordForm.value.bic;
    var iban = this.landlordForm.value.iban;
    var phoneBank = this.landlordForm.value.phoneBank;
    var emailBank = this.landlordForm.value.emailBank;
    var fax = this.landlordForm.value.fax;
    var landlordObject = { landlordName, landlordPhoneWork, landlordPhoneHome, landlordAddress,landlordPhoneMobile, email, bankName, bankAddress, bic, iban, phoneBank, emailBank, fax };
    var jsonObject = JSON.parse(JSON.stringify(landlordObject));
    this.httpClient.post<String>('http://localhost:4600/api/addLandlord', jsonObject, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      }),
	  withCredentials:true
    })
      .subscribe(
        (data: any) => {
          console.log(data);
		  this.router.navigate(['landlords']);
        })

    
  }
}