import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { UserTypeService } from '../user-type.service';

@Component({
  selector: 'app-view-own-tickets',
  templateUrl: './view-own-tickets.component.html',
  styleUrls: ['./view-own-tickets.component.css']
})
export class ViewOwnTicketsComponent implements OnInit {

  ticketsByLandlordId: any;
  id: string;
  private sub: any;

  constructor(private router: ActivatedRoute, private httpClient: HttpClient, private router2: Router, private userTypeService:UserTypeService) { }

  ngOnInit() {
    // this.sub = this.router.params.subscribe(params => {
    //   this.id = params['id'];
    // });
    this.id = this.userTypeService.getUserId();
    this.getData();
  }

  getData() {
    var id = this.id;
    return this.httpClient.post<String>('http://localhost:4600/api/landlord/ticket', JSON.stringify({ id }), {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    })
      .subscribe(
        (data: any) => {
          console.log(data);
          this.ticketsByLandlordId = data;
        });
  }

  delete(id: String) {
    if (confirm("Are you sure to delete this ticket?")) {
      var url = 'http://localhost:4600/api/issues/remove/';
      console.log(id);
      this.httpClient.post<String>(url, JSON.stringify({ id }), {
        headers: new HttpHeaders({
          'Content-Type': 'application/json'
        }),
	  withCredentials:true
      }).subscribe(
        (data: any) => {
          if (data.length != 0) {
            //console.log(data);          
            this.getData();
          }
        }
      )
    }
  }

}
