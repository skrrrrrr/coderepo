import { Component, OnInit } from '@angular/core';
import { NgForm, FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { UserTypeService } from '../user-type.service';
// import {  FileUploader, FileSelectDirective } from 'ng2-file-upload/ng2-file-upload';


@Component({
  selector: 'app-ticket-form',
  templateUrl: './ticket-form.component.html',
  styleUrls: ['./ticket-form.component.css']
})
export class TicketFormComponent implements OnInit {

  categories: Array<any>;
  units: Array<any>;
  userRole: number;
  id: string;
  selectedFile: File = null;
  imagePreview: string | ArrayBuffer;
  fd = new FormData();

  ticketForm = new FormGroup({
    subject: new FormControl(),
    unitName: new FormControl(),
    contactNumber: new FormControl(),
    dateAvailable: new FormControl(),
    hourAvailable: new FormControl(),
    category: new FormControl(),
    subTask: new FormControl(),
    description: new FormControl()
  });

  constructor(private router: Router, private httpClient: HttpClient, private userTypeService: UserTypeService) {
    this.categories = new Array();
    this.units = new Array();
    this.userRole = this.userTypeService.getRole();
    this.id = this.userTypeService.getUserId();
  }

  ngOnInit() {
    this.getIssueCategories();
    this.getUnits();
  }

  //Get issue categories
  getIssueCategories() {
    this.httpClient.post<String>('http://localhost:4600/api/category', {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      }),
      withCredentials: true
    })
      .subscribe(
        (data: any) => {
          console.log(data);
          this.categories = data;
        })
  }

  //Get all Units
  getUnits() {
    if (this.userRole == 1 || this.userRole == 4) {
      this.httpClient.get<String>('http://localhost:4600/api/units', {
        headers: new HttpHeaders({
          'Content-Type': 'application/json'
        }),
        withCredentials: true
      })
        .subscribe(
          (data: any) => {
            console.log(data);
            this.units = data;
          })
    } else if (this.userRole == 2) {
      var id = this.id;
      this.httpClient.post<String>('http://localhost:4600/api/landlord/property', JSON.stringify({ id }), {
        headers: new HttpHeaders({
          'Content-Type': 'application/json'
        }),
        withCredentials: true
      })
        .subscribe(
          (data: any) => {
            console.log(data);
            this.units = data;
          })
    }

  }

  checkValid(){
    return this.ticketForm.value.unitName==null || this.ticketForm.value.unitName=="null" || this.ticketForm.value.category==null || this.ticketForm.value.category=="null";
  }

  onSubmit() {
    var subject = this.ticketForm.value.subject;
    var unitName = this.ticketForm.value.unitName;
    var unit_id = unitName;
    var phone = this.ticketForm.value.contactNumber;
    var dateAvailable = this.ticketForm.value.dateAvailable;
    var hourAvailable = this.ticketForm.value.hourAvailable;
    var category = this.ticketForm.value.category;
    var subTask = this.ticketForm.value.subTask;
    var description = this.ticketForm.value.description;
    var ticketObject = { subject, unit_id, phone, dateAvailable, hourAvailable, category, subTask, description };
    var jsonObject = JSON.parse(JSON.stringify(ticketObject));
    console.log(jsonObject);


    this.httpClient.post<String>('http://localhost:4600/api/issues/raise/', jsonObject, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      }),
      withCredentials: true
    })
      .subscribe(
        (data: any) => {
          console.log(data);
          if (this.userRole == 1) {
            this.router.navigate(['tickets']);
          } else if (this.userRole == 2) {
            this.router.navigate(['properties']);
          } else if (this.userRole == 4) {
            this.router.navigate(['contractor-home']);
          }
        })

  }


   onFileSelected(event){
   this.selectedFile = <File>event.target.files[0];
   const reader = new FileReader();
   reader.onload=()=>{
   	this.imagePreview = reader.result;
   }
   reader.readAsDataURL(this.selectedFile);
   }
}