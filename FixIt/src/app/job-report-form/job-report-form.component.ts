import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { UserTypeService } from '../user-type.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-job-report-form',
  templateUrl: './job-report-form.component.html',
  styleUrls: ['./job-report-form.component.css']
})
export class JobReportFormComponent implements OnInit {

  userRole: number;
  id: string;
  private sub: any;
  ticketId: string;
  ticketDetails: any;

  jobReportForm = new FormGroup({
    dateCompleted: new FormControl(),
    timeCompleted: new FormControl(),
    description: new FormControl()
  });

  constructor(private httpClient: HttpClient, private route: ActivatedRoute, private router2: Router, private userTypeService: UserTypeService) {
    this.userRole = this.userTypeService.getRole();
    this.id = this.userTypeService.getUserId();
  }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      this.ticketId = params['id'];
    });
  }

  onSubmit(){
    var id = this.ticketId;
    var status = 'FINISHED';
    var date_completed = this.jobReportForm.value.dateCompleted;
    var time_completed = this.jobReportForm.value.timeCompleted;
    var description = this.jobReportForm.value.description;
    var jobReport = JSON.parse(JSON.stringify({ date_completed, time_completed, description }));

    if (confirm("Mark this ticket as complete?")) {
    return this.httpClient.post<String>('http://localhost:4600/api/issue/status/change', JSON.stringify({ id, status, jobReport }), {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    })
      .subscribe(
        (data: any) => {
          this.ticketDetails = data;
          this.router2.navigateByUrl('/contractor-home');
        })
  	}
  }

  checkValid(){
    return this.jobReportForm.value.timeCompleted==null || this.jobReportForm.value.timeCompleted=="null" || this.jobReportForm.value.dateCompleted==null || this.jobReportForm.value.dateCompleted=="null";
  }

}
