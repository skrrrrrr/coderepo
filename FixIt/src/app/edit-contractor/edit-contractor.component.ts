import { Component, OnInit } from '@angular/core';
import { NgForm, FormControl, FormGroup } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { debounceTime } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-edit-contractor',
  templateUrl: './edit-contractor.component.html',
  styleUrls: ['./edit-contractor.component.css']
})
export class EditContractorComponent implements OnInit {

  editContractorForm = new FormGroup({
    contractorName: new FormControl(),
    contractorPhoneWork: new FormControl(),
    contractorPhoneHome: new FormControl(),
    contractorPhoneMobile: new FormControl(),
    email: new FormControl(),
    address: new FormControl(),
  });

  id: number;
	private sub: any;
	contractorById: any;
	private _success = new Subject<string>();
	staticAlertClosed = false;
	successMessage: string;

  constructor(private router: ActivatedRoute, private httpClient: HttpClient, private router2: Router) { }

  ngOnInit() {
    this.sub = this.router.params.subscribe(params => {
			this.id = params['id'];
		});
		this.getData();
		setTimeout(() => this.staticAlertClosed = true, 20000);

		this._success.subscribe((message) => this.successMessage = message);
		this._success.pipe(
			debounceTime(10000)
		).subscribe(() => this.successMessage = null);
  }

  getData() {
		var id = this.id;
		return this.httpClient.post<String>('http://localhost:4600/api/contractor', JSON.stringify({ id }), {
			headers: new HttpHeaders({
				'Content-Type': 'application/json'
			}),
	  		withCredentials:true
		})
			.subscribe(
				(data: any) => {
					console.log(data);
					this.contractorById = data;

					this.editContractorForm.patchValue({
						id: data._id,
						contractorName: data.contractor_name,
						contractorPhoneWork: data.contractor_phone_work,
						contractorPhoneHome: data.contractor_phone_home,
						contractorPhoneMobile: data.contractor_phone_mobile,
						email: data.contractor_email,
						address: data.contractor_address,
					});
				})
  }
  
  onSubmit() {
		var id = this.id;
		console.log(id);
		var contractor_name = this.editContractorForm.value.contractorName;
		var contractor_phone_work = this.editContractorForm.value.contractorPhoneWork;
		var contractor_phone_home = this.editContractorForm.value.contractorPhoneHome;
		var contractor_phone_mobile = this.editContractorForm.value.contractorPhoneMobile;
		var contractor_email = this.editContractorForm.value.email;
		var contractor_address = this.editContractorForm.value.address;
		var contractorObject = { contractor_name, contractor_phone_work, contractor_phone_home, contractor_phone_mobile, contractor_email, contractor_address };
		var jsonObject = JSON.parse(JSON.stringify({ id, contractorObject }));
		this.httpClient.post<String>('http://localhost:4600/api/contractors/update/', jsonObject, {
			headers: new HttpHeaders({
				'Content-Type': 'application/json'
			}),
	  		withCredentials:true
		})
			.subscribe(
				(data: any) => {
					this.displayNotification();
					console.log(data);
				});
	}

	public displayNotification() {
		this._success.next('Edit Success');
	}

}
