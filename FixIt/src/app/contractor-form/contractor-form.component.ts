import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { HttpClient ,HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-contractor-form',
  templateUrl: './contractor-form.component.html',
  styleUrls: ['./contractor-form.component.css']
})
export class ContractorFormComponent {

  contractorForm = new FormGroup({
    contractorName: new FormControl(),
    contractorPhoneWork: new FormControl(),
    contractorPhoneHome: new FormControl(),
    contractorPhoneMobile: new FormControl(),
    email: new FormControl(),
    address: new FormControl(),
  });

  constructor(private router: Router, private httpClient: HttpClient) { }

  onSubmit() {
    var contractor_name = this.contractorForm.value.contractorName;
    var contractor_phone_work = this.contractorForm.value.contractorPhoneWork;
    var contractor_phone_home = this.contractorForm.value.contractorPhoneHome;
    var contractor_phone_mobile = this.contractorForm.value.contractorPhoneMobile;
    var contractor_email = this.contractorForm.value.email;
    var contractor_address = this.contractorForm.value.address;
    var contractorObject = { contractor_name, contractor_phone_work, contractor_phone_home, contractor_phone_mobile, contractor_email, contractor_address };
    var jsonObject = JSON.parse(JSON.stringify({contractorObject}));
    this.httpClient.post<String>('http://localhost:4600/api/addContractor', jsonObject, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      }),
	  withCredentials:true
    })
      .subscribe(
        (data: any) => {
          console.log(data);
        })

    this.router.navigate(['contractors']);
    console.log(contractor_phone_work);
  }
}
