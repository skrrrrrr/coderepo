import { Component, OnInit } from '@angular/core';
import { NgForm, FormControl, FormGroup, FormBuilder, Validators, AbstractControl } from '@angular/forms';
import { Router } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-property-form',
  templateUrl: './property-form.component.html',
  styleUrls: ['./property-form.component.css']
})
export class PropertyFormComponent implements OnInit {

  landlords: Array<any>;

  propertyForm = new FormGroup({
    landlord: new FormControl(),
    unitName: new FormControl(),
    address1: new FormControl(),
    address2: new FormControl(),
    address3: new FormControl(),
    unitType: new FormControl(),
    carSpaces: new FormControl(),
    leasedSpaces: new FormControl(),
    financialYearStart: new FormControl(),
    financialYearEnd: new FormControl(),
    vatNo: new FormControl(),
    email: new FormControl(),
    webAddress: new FormControl(),
    financialManagement: new FormControl()
  });
  constructor(private router: Router, private httpClient: HttpClient, private fb: FormBuilder) {
    this.landlords = new Array();
  }

  ngOnInit() {
    this.getLandlords();

  }

  getLandlords(){
    this.httpClient.get<String>('http://localhost:4600/api/landlords', {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      }),
      withCredentials: true
    })
      .subscribe(
        (data: any) => {
          console.log(data);
          this.landlords = data;
        })
  }
  checkValid(){
    return this.propertyForm.value.landlord==null || this.propertyForm.value.landlord=="null" || this.propertyForm.value.unitType==null ||this.propertyForm.value.unitType=="null" ||  this.propertyForm.value.financialManagement==null || this.propertyForm.value.financialManagement=="null";
  }
  onSubmit() {
    //var email = this.loginForm.value.email;
    //this.router.navigate(['admin-home']);
    var landlord_id = this.propertyForm.value.landlord;
    var unitName = this.propertyForm.value.unitName;
    var address1 = this.propertyForm.value.address1;
    var address2 = this.propertyForm.value.address2;
    var address3 = this.propertyForm.value.address3;
    var unitType = this.propertyForm.value.unitType;
    var carSpaces = this.propertyForm.value.carSpaces;
    var leasedSpaces = this.propertyForm.value.leasedSpaces;
    var financialYearStart = this.propertyForm.value.financialYearStart;
    var financialYearEnd = this.propertyForm.value.financialYearEnd;
    var vatNo = this.propertyForm.value.vatNo;
    var email = this.propertyForm.value.email;
    var webAddress = this.propertyForm.value.webAddress;
    var financialManagement = this.propertyForm.value.financialManagement;
    var loginObject = { landlord_id, unitName, address1, address2, address3, unitType, carSpaces, leasedSpaces, financialYearStart, financialYearEnd, vatNo, email, webAddress, financialManagement };
    var jsonObject = JSON.parse(JSON.stringify(loginObject));
    this.httpClient.post<String>('http://localhost:4600/api/addUnit', jsonObject, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      }),
	  withCredentials:true
    })
      .subscribe(
        (data: any) => {
          console.log(data);
        })

    this.router.navigate(['properties']);
  }
}
