import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-view-contractors',
  templateUrl: './view-contractors.component.html',
  styleUrls: ['./view-contractors.component.css']
})
export class ViewContractorsComponent implements OnInit {

  private apiUrl = 'http://localhost:4600/api/contractors';
  data: any = {};
  contractors;
  allContractors;

  searchForm = new FormGroup({
    searchOption: new FormControl(),
    search: new FormControl()
  });

  constructor(private httpClient: HttpClient, private router: Router) { }

  ngOnInit() {
    this.getData();
  }

  checkSearchForm(){
    return this.searchForm.value.searchOption==null || this.searchForm.value.searchOption=="null" || this.searchForm.value.searchOption=="" || 
    this.searchForm.value.search==null || this.searchForm.value.search=="null" || this.searchForm.value.search=="";
  }

  //Get all contractors
  getData() {
    return this.httpClient.get<String>(this.apiUrl, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    })
      .subscribe(
        (data: any) => {
          console.log(data);
          this.contractors = data;
          this.allContractors = data;
        })
  }

  //Delete a contractor
  delete(id: String) {
    if (confirm("Are you sure to delete this contractor?")) {
      var url = 'http://localhost:4600/api/contractors/remove/';

      this.httpClient.post<String>(url, JSON.stringify({ id }), {
        headers: new HttpHeaders({
          'Content-Type': 'application/json'
        }),
        withCredentials: true
      }).subscribe(
        (data: any) => {
          if (data.length != 0) {
            //console.log(data);          
            this.getData();
          }
        }
      )
    }
  }
  reset() {
    // this.getData();
    this.contractors = this.allContractors;
    this.searchForm.reset();
  }
  search() {
    var field = this.searchForm.value.searchOption;
    var value = this.searchForm.value.search;
    var collection = "contractor";
    var jsonObject = JSON.stringify({ field, value, collection });
    return this.httpClient.post<String>('http://localhost:4600/api/search/', jsonObject, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    })
      .subscribe(
        (data: any) => {
          console.log(data);
          this.contractors = data;
        })
  }

}
