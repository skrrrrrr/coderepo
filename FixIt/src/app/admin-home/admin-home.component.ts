import { Component, OnInit } from '@angular/core';
import { NgForm, FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { UserTypeService } from '../user-type.service';

@Component({
  selector: 'app-admin-home',
  templateUrl: './admin-home.component.html',
  styleUrls: ['./admin-home.component.css']
})
export class AdminHomeComponent implements OnInit {

  id: string;
  userRole: number;
  logoutForm = new FormGroup({});
	userData:any = {} ;

  constructor(private router: Router, private httpClient:HttpClient, private userTypeService: UserTypeService) { 
  		// this.getUser();
  		this.userData = {};
  }

  ngOnInit() {
    this.id = this.userTypeService.getUserId();
    this.userRole = this.userTypeService.getRole();
  }
  
  getUser(){
  	this.httpClient.get<String>('http://localhost:4600/api/userDetail', {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      }),
	  withCredentials:true
    })
      .subscribe(
        (data: any) => {
          console.log(data);
          this.userData = data;
        })
  }
  
 }


