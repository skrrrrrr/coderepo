import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';
import { UserTypeService } from '../user-type.service';

@Component({
  selector: 'app-landlord-profile',
  templateUrl: './landlord-profile.component.html',
  styleUrls: ['./landlord-profile.component.css']
})
export class LandlordProfileComponent implements OnInit {

  private sub: any;
  id: string;
  userRole: number;
  landlordId: string;
  landlordDetails: any;

  constructor(private httpClient: HttpClient, private route: ActivatedRoute, private router2: Router, private userTypeService: UserTypeService) { }

  ngOnInit() {
    this.id = this.userTypeService.getUserId();
    this.userRole = this.userTypeService.getRole();
    this.sub = this.route.params.subscribe(params => {
      this.landlordId = params['id'];
    });
    this.getData();
  }

  getData() {
    var id = this.landlordId;
    return this.httpClient.post<String>('http://localhost:4600/api/landlord', JSON.stringify({ id }), {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    })
      .subscribe(
        (data: any) => {
          console.log(data);
          this.landlordDetails = data;
          console.log(this.landlordDetails);
        })
  }

}
