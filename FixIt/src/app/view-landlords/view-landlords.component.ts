import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-view-landlords',
  templateUrl: './view-landlords.component.html',
  styleUrls: ['./view-landlords.component.css']
})
export class ViewLandlordsComponent implements OnInit {

  private apiUrl = 'http://localhost:4600/api/landlords';
  data: any = {};
  landlords;
  allLandlords;

  searchForm = new FormGroup({
    searchOption: new FormControl(),
    search: new FormControl()
  });

  constructor(private httpClient: HttpClient, private router: Router) {
    this.getData();
    this.landlords = new Array();
    this.allLandlords = new Array();
  }

  ngOnInit() { }

  checkSearchForm(){
    return this.searchForm.value.searchOption==null || this.searchForm.value.searchOption=="null" || this.searchForm.value.searchOption=="" || 
    this.searchForm.value.search==null || this.searchForm.value.search=="null" || this.searchForm.value.search=="";
  }

  //Get all landlords info from api
  getData() {
    return this.httpClient.get<String>(this.apiUrl, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    })
      .subscribe(
        (data: any) => {
          console.log(data);
          this.landlords = data;
          this.allLandlords = data;
        })
  }

  //Delete a landlord
  delete(id: String) {
    if (confirm("Are you sure to delete this landlord?")) {
      var url = 'http://localhost:4600/api/landlords/remove/';

      this.httpClient.post<String>(url, JSON.stringify({ id }), {
        headers: new HttpHeaders({
          'Content-Type': 'application/json'
        }),
        withCredentials: true
      }).subscribe(
        (data: any) => {
          if (data.length != 0) {
            //console.log(data);          
            this.getData();
          }
        }
      )
    }
  }
  reset() {
    // this.getData();
    this.landlords = this.allLandlords;
    this.searchForm.reset();
  }

  search() {
    var field = this.searchForm.value.searchOption;
    var value = this.searchForm.value.search;
    var collection = "landlord";
    var jsonObject = JSON.stringify({ field, value, collection });
    return this.httpClient.post<String>('http://localhost:4600/api/search/', jsonObject, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    })
      .subscribe(
        (data: any) => {
          console.log(data);
          this.landlords = data;
        })
  }

  navigate() {
    this.router.navigateByUrl("['/edit-landlord', landlord._id]");
  }

}
