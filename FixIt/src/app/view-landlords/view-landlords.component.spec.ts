import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewLandlordsComponent } from './view-landlords.component';

describe('ViewLandlordsComponent', () => {
  let component: ViewLandlordsComponent;
  let fixture: ComponentFixture<ViewLandlordsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewLandlordsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewLandlordsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
