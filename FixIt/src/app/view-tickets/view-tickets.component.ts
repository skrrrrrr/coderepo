import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { UserTypeService } from '../user-type.service';

@Component({
  selector: 'app-view-tickets',
  templateUrl: './view-tickets.component.html',
  styleUrls: ['./view-tickets.component.css']
})
export class ViewTicketsComponent implements OnInit {

  private apiUrl = 'http://localhost:4600/api/issues';
  data: any = {};
  id: string;
  userRole: number;
  tickets;
  newTickets;
  allTickets;

  searchForm = new FormGroup({
    searchOption: new FormControl(),
    search: new FormControl()
  });

  filterForm = new FormGroup({
    filterOption: new FormControl()
  });

  sortForm = new FormGroup({
    sortOption: new FormControl()
  });

  constructor(private httpClient: HttpClient, private router: Router, private userTypeService: UserTypeService) {
    this.getData();
    this.getNewTicket();
    this.tickets = new Array();
    this.newTickets = new Array();
    this.allTickets = new Array();
  }

  ngOnInit() {
    this.id = this.userTypeService.getUserId();
    this.userRole = this.userTypeService.getRole();
  }

  checkSearchForm(){
    return this.searchForm.value.searchOption==null || this.searchForm.value.searchOption=="null" || this.searchForm.value.searchOption=="" || 
    this.searchForm.value.search==null || this.searchForm.value.search=="null" || this.searchForm.value.search=="";
  }

  checkFilterForm(){
    return this.filterForm.value.filterOption==null || this.filterForm.value.filterOption=="null" || this.filterForm.value.filterOption=="";
  }

  checkSortForm(){
    return this.sortForm.value.sortOption==null || this.sortForm.value.sortOption=="null" || this.sortForm.value.sortOption=="";
  }

  //Get all tickets from api
  getData() {
    return this.httpClient.post<String>(this.apiUrl, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    })
      .subscribe(
        (data: any) => {
          console.log(data);
          this.tickets = data;
          this.allTickets = data;
        })
  }

  getNewTicket() {
    return this.httpClient.post<String>('http://localhost:4600/api/issues/filter/status', JSON.stringify({ status: "CREATED" }), {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    })
      .subscribe(
        (data: any) => {
          console.log(data);
          this.newTickets = data;
        })
  }

  delete(ticketId: String) {
    if (confirm("Are you sure to delete this ticket?")) {
      var url = 'http://localhost:4600/api/issues/remove/';
      var temp = ticketId;
      console.log(temp);
      this.httpClient.post<String>(url, JSON.stringify({ temp }), {
        headers: new HttpHeaders({
          'Content-Type': 'application/json'
        }),
        withCredentials: true
      }).subscribe(
        (data: any) => {
          if (data.length != 0) {
            console.log(data);          
            this.getData();
            this.getNewTicket();
          }
        }
      )
    }
  }

  search() {
    var field = this.searchForm.value.searchOption;
    var value = this.searchForm.value.search;
    var collection = "ticket";
    var jsonObject = JSON.stringify({ field, value, collection });
    return this.httpClient.post<String>('http://localhost:4600/api/search/', jsonObject, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    })
      .subscribe(
        (data: any) => {
          console.log(data);
          this.tickets = data;
        })
  }

  reset() {
    // this.getData();
    this.tickets = this.allTickets;
    this.searchForm.reset();
    this.filterForm.reset();
    this.sortForm.reset();

  }

  filter() {
    console.log(this.filterForm.value.filterOption);
    var status = this.filterForm.value.filterOption;
    "http://localhost:4600/api/issues/filter/status"

    return this.httpClient.post<String>('http://localhost:4600/api/issues/filter/status', JSON.stringify({ status }), {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    })
      .subscribe(
        (data: any) => {
          console.log(data);
          this.tickets = data;
        })

  }

  sort()
  {  
    console.log(this.sortForm.value.sortOption);
      
    return this.httpClient.post<String>(this.apiUrl, JSON.stringify({sortOption:this.sortForm.value.sortOption}),{
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    })
      .subscribe(
        (data: any) => {
          console.log(data);
          this.tickets = data;
        })
  }
}
