import { Component, OnInit } from '@angular/core';
import { UserTypeService } from '../user-type.service';
import { Router } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-contractor-home',
  templateUrl: './contractor-home.component.html',
  styleUrls: ['./contractor-home.component.css']
})
export class ContractorHomeComponent implements OnInit {
  id: string;
  userRole: number;
  data:any ={};
  tickets;

  constructor(private userTypeService:UserTypeService,private httpClient: HttpClient, private router: Router) {
  	
  }

  ngOnInit() {
    this.id = this.userTypeService.getUserId();
    this.userRole = this.userTypeService.getRole();
	this.getData();
  }
  
  getData() {
  	var id = this.id;
	console.log(id);
    return this.httpClient.post<String>('http://localhost:4600/api/contractor/ticket/',JSON.stringify({ id }), {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    })
      .subscribe(
        (data: any) => {
          console.log(data);
          this.tickets = data;
        })
  }

}
