import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { FormGroup, FormControl } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { debounceTime } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { UserTypeService } from '../user-type.service';

@Component({
  selector: 'app-edit-ticket',
  templateUrl: './edit-ticket.component.html',
  styleUrls: ['./edit-ticket.component.css']
})
export class EditTicketComponent implements OnInit {

  categories: Array<any>;

  ticketForm = new FormGroup({
    subject: new FormControl(),
    // contractor: new FormControl(),
    unitName: new FormControl(),
    contactNumber: new FormControl(),
    dateAvailable: new FormControl(),
    hourAvailable: new FormControl(),
    category: new FormControl(),
    // subTask: new FormControl(),
    description: new FormControl()
  })

  id: string;
  ticketId: string;
  private sub: any;
  ticketById: any;
  private _success = new Subject<string>();
  staticAlertClosed = false;
  successMessage: string;
  userRole: number;


  constructor(private httpClient: HttpClient, private route: ActivatedRoute, private router2: Router, private userTypeService:UserTypeService) {
    this.categories = new Array();
  }
  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      this.ticketId = params['id'];
    });
    this.id = this.userTypeService.getUserId();
    this.userRole = this.userTypeService.getRole();
    this.getData();

    console.log("here"+this.userRole);

    setTimeout(() => this.staticAlertClosed = true, 20000);

    this._success.subscribe((message) => this.successMessage = message);
    this._success.pipe(
      debounceTime(10000)
    ).subscribe(() => this.successMessage = null);

    this.getIssueCategories();
  }

  //Get issue categories
  getIssueCategories() {
    this.httpClient.post<String>('http://localhost:4600/api/category', {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      }),
	  withCredentials:true
    })
      .subscribe(
        (data: any) => {
          console.log(data);
          this.categories = data;
        })
  }


  //Get ticket info by id from api
  getData() {
    var id = this.ticketId;
    return this.httpClient.post<String>('http://localhost:4600/api/issue', JSON.stringify({ id }), {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    })
      .subscribe(
        (data: any) => {
          console.log(data);
          this.ticketById = data;

          this.ticketForm.patchValue({
            id: data._id,
            subject: data.subject == null || data.subject == undefined ? "" : this.ticketById.subject,
            unitName: data.unitName == null || data.unitName == undefined ? "" : this.ticketById.unitName,
            contactNumber: data.phone,
            dateAvailable: data.dateAvailable,
            hourAvailable: data.hourAvailable,
            category: data.category,
            description: data.description,
            // contractor: data.contractor_id
          });

        })
  }

  onSubmit() {
    var id = this.id;
    console.log(id);
    var subject = this.ticketForm.value.subject;
    var unitName = this.ticketForm.value.unitName;
    var phone = this.ticketForm.value.contactNumber;
    var dateAvailable = this.ticketForm.value.dateAvailable;
    var hourAvailable = this.ticketForm.value.hourAvailable;
    var category = this.ticketForm.value.category;
    var description = this.ticketForm.value.description;

    var ticketObject = { subject, unitName, phone, dateAvailable, hourAvailable, category, description };
    var jsonObject = JSON.parse(JSON.stringify({ id, ticketObject }));
    this.httpClient.post<String>('http://localhost:4600/api/issues/update/', jsonObject, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      }),
	  withCredentials:true
    }).subscribe((data: any) => {
      console.log(data);
      this.displayNotification();
      // can implement popup message later. 
    });
    console.log(this.ticketById);
  }

  public displayNotification() {
    this._success.next('Edit Success');
  }
}



