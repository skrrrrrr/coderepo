import { Component, OnInit } from '@angular/core';
import { NgForm, FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { UserTypeService }from '../user-type.service';

@Component({
  selector: 'app-view-property',
  templateUrl: './view-property.component.html',
  styleUrls: ['./view-property.component.css']
})
export class ViewPropertyComponent implements OnInit {
  //Class for property
  unitsByLandlordId: any;
  id: string;
  userRole: number;
  properties;
  allProperties;
  private sub: any;
  data: any = {};

  searchForm = new FormGroup({
    searchOption: new FormControl(),
    search: new FormControl()
  });

  constructor(private httpClient: HttpClient, private router: Router, private userTypeService: UserTypeService) {
    this.getData();
    this.getLandlordUnit();
    this.properties = new Array();
  }

  ngOnInit() { 
    this.userRole = this.userTypeService.getRole();
  }

  checkSearchForm(){
    return this.searchForm.value.searchOption==null || this.searchForm.value.searchOption=="null" || this.searchForm.value.searchOption=="" || 
    this.searchForm.value.search==null || this.searchForm.value.search=="null" || this.searchForm.value.search=="";
  }

  //Get all properties info from api
  getData() {
    return this.httpClient.get<String>('http://localhost:4600/api/units', {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    })
      .subscribe(
        (data: any) => {
          console.log(data);
          this.properties = data;
          this.allProperties = data;
        })
  }

  getLandlordUnit()
  {
    var landlord_id = this.userTypeService.getUserId();
    return this.httpClient.post<String>('http://localhost:4600/api/landlord/property', JSON.stringify({id:landlord_id}), {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    })
      .subscribe(
        (data: any) => {
          console.log(data);
          this.unitsByLandlordId = data;
        })
  }

  reset() {
    // this.getData();
    this.properties = this.allProperties;
    this.searchForm.reset();
  }
  //Delete a property
  delete(id: String) {
    if (confirm("Are you sure to delete this property?")) {
      var url = 'http://localhost:4600/api/properties/remove/';

      this.httpClient.post<String>(url, JSON.stringify({ id }), {
        headers: new HttpHeaders({
          'Content-Type': 'application/json'
        }),
	  withCredentials:true
      }).subscribe(
        (data: any) => {
          if (data.length != 0) {
            //console.log(data);          
            this.getData();
          }
        }
      )
    }
  }

  search(){
    var field = this.searchForm.value.searchOption;
    var value = this.searchForm.value.search;
    var collection = "unit";
    var jsonObject = JSON.stringify({field, value, collection});
    return this.httpClient.post<String>('http://localhost:4600/api/search/', jsonObject, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    })
      .subscribe(
        (data: any) => {
          console.log(data);
          this.properties = data;
        })  }

}