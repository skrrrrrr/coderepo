import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';
import { UserTypeService } from '../user-type.service';
import { getViewData } from '@angular/core/src/render3/state';

@Component({
  selector: 'app-view-ticket-details',
  templateUrl: './view-ticket-details.component.html',
  styleUrls: ['./view-ticket-details.component.css']
})
export class ViewTicketDetailsComponent implements OnInit {

  private sub: any;
  ticketId: string;
  id: string;
  ticketDetails: any;
  userRole:number;

  constructor(private httpClient: HttpClient, private route: ActivatedRoute, private router2: Router, private userTypeService:UserTypeService) { }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      this.ticketId = params['id'];
    });
    this.id = this.userTypeService.getUserId();
    this.getData();
	this.userRole = this.userTypeService.getRole();
  }

  getData(){
    var id = this.ticketId;
    return this.httpClient.post<String>('http://localhost:4600/api/issue', JSON.stringify({ id }), {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    })
      .subscribe(
        (data: any) => {
          console.log(data);
          this.ticketDetails = data;
          console.log(this.ticketDetails);
        })
  }

  approve(){
    var id = this.ticketId;
    var status = 'OPEN';
    if (confirm("Approve this ticket?")) {
    return this.httpClient.post<String>('http://localhost:4600/api/issue/status/change', JSON.stringify({ id, status }), {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    })
      .subscribe(
        (data: any) => {
          this.ticketDetails = data;
          this.router2.navigateByUrl('/tickets');
        })
  		}
	}

accept(){
    var id = this.ticketId;
    var status = 'IN_PROGRESS';
    if (confirm("Accept this ticket?")) {
    return this.httpClient.post<String>('http://localhost:4600/api/issue/status/change', JSON.stringify({ id, status }), {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    })
      .subscribe(
        (data: any) => {
          this.ticketDetails = data;
          this.router2.navigateByUrl('/contractor-home');
        })
  	}
  }
  
  reject(){
    var id = this.ticketId;
    var status = 'REJECTED';
    if (confirm("Reject this ticket?")) {
    return this.httpClient.post<String>('http://localhost:4600/api/issue/status/change', JSON.stringify({ id, status }), {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    })
      .subscribe(
        (data: any) => {
          this.ticketDetails = data;
          this.router2.navigateByUrl('/contractor-home');
        })
  	}
  }

  complete(){
    var id = this.ticketId;
    var status = 'FINISHED';
    if (confirm("Mark this ticket as complete?")) {
    return this.httpClient.post<String>('http://localhost:4600/api/issue/status/change', JSON.stringify({ id, status }), {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    })
      .subscribe(
        (data: any) => {
          this.ticketDetails = data;
          this.router2.navigateByUrl('/contractor-home');
        })
  	}
  }

  reassign(){
    var id = this.ticketId;
    var status = 'REASSIGN';
    if (confirm("Reassign this ticket to another contractor?")) {
      return this.httpClient.post<String>('http://localhost:4600/api/issue/status/change', JSON.stringify({ id, status }), {
        headers: new HttpHeaders({
          'Content-Type': 'application/json'
        })
      })
        .subscribe(
          (data: any) => {
            this.ticketDetails = data;
            this.router2.navigate(['/assign-contractor', id]);
          })
      }
  }

  close(){
    var id = this.ticketId;
    var status = 'CLOSED';
    if (confirm("Close this ticket?")) {
    return this.httpClient.post<String>('http://localhost:4600/api/issue/status/change', JSON.stringify({ id, status }), {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    })
      .subscribe(
        (data: any) => {
          this.ticketDetails = data;
          this.router2.navigateByUrl('/tickets');
        })
  	}
  }

}
