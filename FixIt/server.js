var express = require('express');
var session = require('express-session');
var path = require('path');
var bodyParser = require('body-parser');
var cors = require('cors');

var pages = require('./routes/pages');
var apis = require('./routes/apis');
var port = 4600;
var app = express();

app.set('view engine', 'ejs');

//view engine
app.set('views',path.join(__dirname,'src/app'));
app.set('view engine','ejs');
app.engine('html', require('ejs').renderFile);
// res.sendFile('index.html', { root : VIEWS });

//set up session
app.use(session({
	secret: 'project',
	cookie: {
		expires: 24*60*60*1000
	}
}));

//set Static folder
app.use(express.static(path.join(__dirname,'src')));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:false}));

// should display whats in pages router
app.use('/',pages);

app.use(cors({
	origin :['http://localhost:4200'],
	credentials:true
}));

// should display whats in api router
app.use('/api',apis);

app.use(function (err, req, res, next) {
  console.error(err.stack)
  res.status(500).send('Something broke!')
})


app.listen(port, function(){
  console.log('Server started on port '+ port);
});

module.exports = app;
